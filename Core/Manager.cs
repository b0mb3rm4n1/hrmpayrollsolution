﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base;
using Data;

namespace Core
{
    /// <summary>
    /// The middle-level Worker category within the organization's structure, derives all of its properties from the base Person class
    /// </summary>
    public class Manager : Person
    {
        /// <summary>ListOfInferiorEmployees is a list of inferiors assigned to the Manager object</summary>
        public List<Employee> ListOfInferiorEmployees { get; }
        /// <summary>_InferiorEmployee is a sub-Person object used to access specific class methods that calculate that Person's payroll</summary>
        private Person _InferiorEmployee { get; }
        
        /// <summary>
        /// The constructor method assigns values to Manager's basic characteristics and fills the list of inferiors right away
        /// </summary>
        /// <param name="memberID">Worker's ID</param>
        /// <param name="fullName">Worker's full name</param>
        /// <param name="dateOfEmployment">Worker's date of employment</param>
        /// <param name="position">Worker's position</param>
        /// <param name="baseRate">How much Worker earns per hour</param>
        public Manager(int memberID, string fullName, DateTime dateOfEmployment, string position, decimal baseRate) : base(memberID, fullName, dateOfEmployment, position, baseRate)
        {
            ListOfInferiorEmployees = new List<Employee>();

            try
            {
                foreach (var inferior in PersonFetcher.FetchInferiors(memberID))
                {
                    // Casting inferiors to their appropriate type in order to accurately calculate their payrolls later on
                    _InferiorEmployee = ClassConverter.ConvertBaseToDerived(inferior);
                    // and adding them to the list (making sure that they are correctly identified)
                    ListOfInferiorEmployees.Add(_InferiorEmployee as Employee);
                }
            }
            catch (Exception)
            {
                throw new Exception("Unable to load elements");
            }
        }

        /// <summary>
        /// Calculates Manager's due pay for the period between the two incoming dates
        /// </summary>
        /// <param name="startDate">The date from which Manager's due pay is to be calculated</param>
        /// <param name="endDate">The date to which Manager's due pay is to be calculated</param>
        /// <returns> A 3-decimal array which is later broken down and displayed in a granular manner</returns>
        public override decimal[] CalculatePay(DateTime startDate, DateTime endDate)
        {
            var workDays = AdjustTime(startDate, endDate);
            // Manager plan: base pay + 5% per each year of employment (but no more than 40% of base pay) + 0.5% of pay of each level one inferior
            decimal[] combinedPay = new decimal[3];
            decimal baseOneDaysWages = BaseHourlyRate * WorkHoursPerDay;
            decimal bonusPayPerDay = 0.05M * YearsOfEmploymentByPayrollsDate * baseOneDaysWages;
            decimal maxBonusPayPerDay = 0.4M * baseOneDaysWages;
            // combinedPay[0] is base pay over the specified time period
            combinedPay[0] = baseOneDaysWages * workDays;
            // combinedPay[1] is year-based bonus pay over the specified time period
            combinedPay[1] = ((bonusPayPerDay > maxBonusPayPerDay) ? maxBonusPayPerDay : bonusPayPerDay) * workDays;
            // combinedPay[2] is referral pay for all Employees under supervision over the same time period
            combinedPay[2] = CalculateReferralPay(PayrollStartDate, PayrollEndDate); 
            return combinedPay;
        }

        /// <summary>
        /// Calculates how much money Manager's inferiors earned during the period between the specified dates
        /// </summary>
        /// <param name="from">The date from which each inferior's due pay is to be calculated</param>
        /// <param name="to">The date to which each inferior's due pay is to be calculated</param>
        /// <returns>Manager's due referral bonus (0.5% of all their inferiors' pay) for the specified period</returns>
        private decimal CalculateReferralPay(DateTime from, DateTime to)
        {
            decimal referralPay = default(decimal);

            foreach (var inferior in ListOfInferiorEmployees)
            {
                referralPay = referralPay + inferior.CalculatePay(from, to).Sum();
            }
            return referralPay * 0.005M;
        }
    }
}
