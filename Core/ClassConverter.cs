﻿using Base;

namespace Core
{
    /// <summary>
    /// Static utility class that helps cast objects from Person to Employee/Manager/Salesman
    /// </summary>
    public static class ClassConverter
    {
        /// <summary>
        /// Creates and returns a new object of a derived class with the same values as the incoming base class object
        /// </summary>
        /// <param name="person">Person object whose values are copied</param>
        /// <returns>An Employee/Manager/Salesman object with identical properties as the incoming Person object</returns>
        public static dynamic ConvertBaseToDerived(Person person)
        {
            if (person.Position == "Employee")
            {
                return new Employee(person.MemberID, person.FullName, person.DateOfEmployment, person.Position, person.BaseHourlyRate);
            }
            else if (person.Position == "Manager")
            {
                return new Manager(person.MemberID, person.FullName, person.DateOfEmployment, person.Position, person.BaseHourlyRate);
            }
            else if (person.Position == "Salesman")
            {
                return new Salesman(person.MemberID, person.FullName, person.DateOfEmployment, person.Position, person.BaseHourlyRate);
            }
            else return person;
        }
    }
}
