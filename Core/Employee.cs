﻿using System;
using Base;

namespace Core
{
    /// <summary>
    /// The lowest-level Worker category within the organization's structure, derives its properties from the base Person class
    /// </summary>
    public class Employee : Person
    {
        /// <summary>
        /// The constructor method assigns values to Employee's basic characteristics
        /// </summary>
        /// <param name="memberID">Worker's ID</param>
        /// <param name="fullName">Worker's full name</param>
        /// <param name="dateOfEmployment">Worker's date of employment</param>
        /// <param name="position">Worker's position</param>
        /// <param name="baseRate">How much Worker earns per hour</param>
        public Employee(int memberID, string fullName, DateTime dateOfEmployment, string position, decimal baseRate) : base(memberID, fullName, dateOfEmployment, position, baseRate)
        {
            // Everything is redirected to the base class
        }

        /// <summary>
        /// Calculates Employee's due pay for the period between the two incoming dates
        /// </summary>
        /// <param name="startDate">The date from which Employee's due pay is to be calculated</param>
        /// <param name="endDate">The date to which Employee's due pay is to be calculated</param>
        /// <returns> A 3-decimal array which is later broken down and displayed in a granular manner</returns>
        public override decimal[] CalculatePay(DateTime startDate, DateTime endDate)
        {
            var workDays = AdjustTime(startDate, endDate);
            // Employee plan: base pay + 3% per each year of employment (but no more than 30% of base pay)
            decimal[] combinedPay = new decimal[3];
            decimal baseOneDaysWages = BaseHourlyRate * WorkHoursPerDay;
            decimal bonusPayPerDay = 0.03M * YearsOfEmploymentByPayrollsDate * baseOneDaysWages;
            decimal maxBonusPayPerDay = 0.3M * baseOneDaysWages;
            // combinedPay[0] is base pay over the specified time period
            combinedPay[0] = baseOneDaysWages * workDays;
            // combinedPay[1] is year-based bonus pay over the specified time period
            combinedPay[1] = ((bonusPayPerDay > maxBonusPayPerDay) ? maxBonusPayPerDay : bonusPayPerDay) * workDays; 
            // combinedPay[2] is referral pay, always 0 for Employees since they don't have inferiors
            combinedPay[2] = default(decimal); 
            return combinedPay;
        }
    }
}
