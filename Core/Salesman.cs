﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base;
using Data;

namespace Core
{
    /// <summary>
    /// The highest-level Worker category within the organization's structure, derives its properties from the base Person class
    /// </summary>
    public class Salesman : Person
    {
        /// <summary>ListOfInferiorPersons is a list of inferiors assigned to the Salesman object</summary>
        public List<Person> ListOfInferiorPersons { get; }
        /// <summary>_InferiorEmployee is a sub-Person object used to access specific class methods that calculate that Person's payroll</summary>
        private Person _InferiorPerson { get; }

        /// <summary>
        /// The constructor method assigns values to Salesman's basic characteristics and fills the list of inferiors right away
        /// </summary>
        /// <param name="memberID">Worker's ID</param>
        /// <param name="fullName">Worker's full name</param>
        /// <param name="dateOfEmployment">Worker's date of employment</param>
        /// <param name="position">Worker's position</param>
        /// <param name="baseRate">How much Worker earns per hour</param>
        public Salesman(int memberID, string fullName, DateTime dateOfEmployment, string position, decimal baseRate) : base(memberID, fullName, dateOfEmployment, position, baseRate)
        {
            ListOfInferiorPersons = new List<Person>();

            try
            {
                foreach (var inferior in PersonFetcher.FetchInferiors(memberID))
                {
                    // Casting inferiors to their appropriate type in order to accurately calculate their payrolls later on
                    _InferiorPerson = ClassConverter.ConvertBaseToDerived(inferior);

                    // And adding them to the list (making sure that they are correctly identified)
                    if (_InferiorPerson.Position == "Employee")
                    {
                        ListOfInferiorPersons.Add(_InferiorPerson as Employee);
                    }
                    else if (_InferiorPerson.Position == "Manager")
                    {
                        ListOfInferiorPersons.Add(_InferiorPerson as Manager);
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Unable to load elements");
            }
        }

        /// <summary>
        /// Calculates Salesman's due pay for the period between the two incoming dates
        /// </summary>
        /// <param name="startDate">The date from which Salesman's due pay is to be calculated</param>
        /// <param name="endDate">The date to which Salesman's due pay is to be calculated</param>
        /// <returns> A 3-decimal array which is later broken down and displayed in a granular manner</returns>
        public override decimal[] CalculatePay(DateTime startDate, DateTime endDate)
        {
            var workDays = AdjustTime(startDate, endDate);
            // Salesman plan: base pay + 1% per each year of employment (but no more than 35% of base pay) + 0.3% of pay of each inferior of any level
            decimal[] combinedPay = new decimal[3];
            decimal baseOneDaysWages = BaseHourlyRate * WorkHoursPerDay;
            decimal bonusPayPerDay = 0.01M * YearsOfEmploymentByPayrollsDate * baseOneDaysWages;
            decimal maxBonusPayPerDay = 0.35M * baseOneDaysWages;
            // combinedPay[0] is base pay over the specified time period
            combinedPay[0] = baseOneDaysWages * workDays; 
            // combinedPay[1] is year-based bonus pay over the specified time period
            combinedPay[1] = ((bonusPayPerDay > maxBonusPayPerDay) ? maxBonusPayPerDay : bonusPayPerDay) * workDays; 
            // combinedPay[2] is referral pay for all Persons under supervision over the same time period
            combinedPay[2] = CalculateReferralPay(PayrollStartDate, PayrollEndDate);
            return combinedPay;
        }

        /// <summary>
        /// Calculates how much money Salesman's inferiors earned during the period between the specified dates
        /// </summary>
        /// <param name="from">The date from which each inferior's due pay is to be calculated</param>
        /// <param name="to">The date to which each inferior's due pay is to be calculated</param>
        /// <returns>Salesman's due referral bonus (0.3% of all their inferiors' pay) for the specified period</returns>
        private decimal CalculateReferralPay(DateTime from, DateTime to)
        {
            decimal referralPay = default(decimal);

            foreach (var inferior in ListOfInferiorPersons)
            {
                referralPay = referralPay + inferior.CalculatePay(from, to).Sum();
            }
            return referralPay * 0.003M;
        }
    }
}
