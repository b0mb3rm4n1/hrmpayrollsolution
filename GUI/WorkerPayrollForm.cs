﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Base;
using Data;
using Core;
using Service;

namespace GUI
{
    /// <summary>
    /// Form where Worker can see information concerning themselves and their inferiors
    /// </summary>
    public partial class WorkerPayrollForm : Form
    {
        /// <summary>_Worker is Person/Worker who logged in</summary>
        private Person _Worker { get; set; }
        /// <summary>_Inferior is Worker's inferior whose information will be displayed</summary>
        private Person _Inferior { get; set; }
        /// <summary>_WorkerView is a switch that is used to determine whose information to show</summary>
        private bool _WorkerView { get; set; }
        /// <summary>_InferiorView is a switch that is used to determine whose information to show</summary>
        private bool _InferiorView { get; set; }
        /// <summary>workerPayrollForm is the only allowed instance of WorkerPayrollForm</summary>
        private static WorkerPayrollForm workerPayrollForm { get; set; }

        /// <summary>
        /// Fetches or creates WorkerPayrollForm instance
        /// </summary>
        /// <returns>The existing instance of WorkerPayrollForm or a new WorkerPayrollForm if such doesn't exist</returns>
        public static WorkerPayrollForm GetWorkerPayrollForm()
        {
            if (workerPayrollForm == null) {
                return new WorkerPayrollForm();
            }
            else
            {
                return workerPayrollForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of WorkerPayrollForm elements and customizes the form to display logged-in Worker's information
        /// </summary>
        private WorkerPayrollForm()
        {
            InitializeComponent();
            // WorkerPayrollForm is personalized from the start
            _Worker = PersonFetcher.FetchPerson(EntityIdHolder.WorkerID).First();
            // List of inferior Workers is put together
            LoadListOfInferiors();
            _WorkerView = true;
            _InferiorView = false;
            DisplayPersonInfo();
        }

        /// <summary>
        /// Unveils hidden fields when there's new information to display
        /// </summary>
        private void ShowFields()
        {
            totalWorkdays_label.Visible = true;
            totalWorkdays_textBox.Visible = true;
            totalEarnings_label.Visible = true;
            totalEarnings_textBox.Visible = true;
            basePay_label.Visible = true;
            basePay_textBox.Visible = true;
            yearBonus_label.Visible = true;
            yearBonus_textBox.Visible = true;
            referralBonusPay_label.Visible = true;
            referralBonusPay_textBox.Visible = true;
        }

        /// <summary>
        /// Hides fields when switching between WorkerView and InferiorView
        /// </summary>
        private void HideFields()
        {
            totalWorkdays_label.Visible = false;
            totalWorkdays_textBox.Visible = false;
            totalEarnings_label.Visible = false;
            totalEarnings_textBox.Visible = false;
            basePay_label.Visible = false;
            basePay_textBox.Visible = false;
            yearBonus_label.Visible = false;
            yearBonus_textBox.Visible = false;
            referralBonusPay_label.Visible = false;
            referralBonusPay_textBox.Visible = false;
        }

        /// <summary>
        /// Displays information about Worker or one of Worker's inferiors depending on context
        /// </summary>
        private void DisplayPersonInfo()
        {
            var personInFocus = default(Person);
            try
            {
                if (_WorkerView == true && _InferiorView == false)
                {
                    _Worker = ClassConverter.ConvertBaseToDerived(_Worker);
                    personInFocus = _Worker;
                    // Header message is adusted depending on whose information is shown
                    hello_label.Text = $"Alright, {_Worker.FullName.Split(' ')[0]}! Here's your info: ";
                    // Turns the display of information about Person on and off depending on their position
                    if (personInFocus.Position != "Employee")
                    {
                        supervises_label.Visible = true;
                        supervisedInferiors_comboBox.Visible = true;
                    }
                    else
                    {
                        supervises_label.Visible = false;
                        supervisedInferiors_comboBox.Visible = false;
                    }
                    goBackToWorkerProfile_button.Visible = false;
                }
                else
                {
                    _Inferior = ClassConverter.ConvertBaseToDerived(_Inferior);
                    personInFocus = _Inferior;
                    // Header message is adusted depending on whose information is shown
                    hello_label.Text = $"Okay, {_Worker.FullName.Split(' ')[0]}!\nHere's the info about {_Inferior.FullName}:";
                    goBackToWorkerProfile_button.Visible = true;
                }
                // In any case, the text fields display information about the current Person in focus
                memberID_textBox.Text = $"{personInFocus.MemberID.ToString()}";
                fullName_textBox.Text = $"{personInFocus.FullName}";
                dateOfEmployment_textBox.Text = $"{personInFocus.DateOfEmployment.ToShortDateString()}";
                position_textBox.Text = $"{personInFocus.Position}";
                baseRate_textBox.Text = $"{personInFocus.BaseHourlyRate.ToString()}";
                yearsWithCompany_textBox.Text = personInFocus.GetYearsOfEmployment(endDate_dateTimePicker.Value).ToString();
                // The DateTimePicker objects' Min and Max Dates are immediately adjusted to reflect Person's employment time windows
                startDate_dateTimePicker.MinDate = DateTime.Parse(personInFocus.DateOfEmployment.ToString());
                startDate_dateTimePicker.MaxDate = DateTime.Now;
                endDate_dateTimePicker.MinDate = DateTime.Parse(personInFocus.DateOfEmployment.ToString());
                endDate_dateTimePicker.MaxDate = DateTime.Now;
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Resets the values currently held by the DateTimePicker controls
        /// </summary>
        private void ResetCalendars()
        {
            startDate_dateTimePicker.Value = startDate_dateTimePicker.MaxDate;
            endDate_dateTimePicker.Value = endDate_dateTimePicker.MaxDate;
        }

        /// <summary>
        /// Refreshes certain UI elements depending on context
        /// </summary>
        private void UpdateWorkdays()
        {
            // If an illegal time span is selected, the DateTimePicker objects' values are reset
            if (startDate_dateTimePicker.Value > endDate_dateTimePicker.Value)
            {
                ResetCalendars();
            }
            // If the time span is OK, the number of workdays between the two dates is calculated and displayed
            else
            {
                totalWorkdays_textBox.Text = WorkdayCounter.CountWorkdays(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value).ToString();
            }
        }

        /// <summary>
        /// Refreshes the UI to reflect how many years Worker had worked for the company by the date selected in the enddate DateTimePicker control
        /// </summary>
        private void AdjustYearsWithCompany()
        {
            if (_Worker != null && _WorkerView == true && _InferiorView == false)
            {
                yearsWithCompany_textBox.Text = _Worker.GetYearsOfEmployment(endDate_dateTimePicker.Value).ToString();
            }
            else if (_Worker != null && _WorkerView == false && _InferiorView == true)
            {
                yearsWithCompany_textBox.Text = _Inferior.GetYearsOfEmployment(endDate_dateTimePicker.Value).ToString();
            }
        }

        /// <summary>
        /// Adds information about Worker's inferiors into the appropriate comboBox control
        /// </summary>
        private void LoadListOfInferiors()
        {
            var sb = new StringBuilder();

            try
            {
                // Iterates over inferior objects assigned to _Worker
                foreach (var person in PersonFetcher.FetchInferiors(_Worker.MemberID))
                {
                    // Adds each one to the comboBox control
                    supervisedInferiors_comboBox.Items.Add($"{person.MemberID}: {person.FullName} - {person.Position}");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Obtains payroll info from Person's method and displays the info in the appropriate fields
        /// </summary>
        private void ShowPayrollResults()
        {
            var personInFocus = default(Person);
            if (_WorkerView == true && _InferiorView == false)
            {
                personInFocus = _Worker;
            }
            else
            {
                personInFocus = _Inferior;
            }
            try
            {
                // A 3-index decimal array is returned
                var calculatedPay = personInFocus.CalculatePay(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value);
                // Two text fields common to all Person subclass objects are updated
                basePay_textBox.Text = calculatedPay[0].ToString("0.##");
                yearBonus_textBox.Text = calculatedPay[1].ToString("0.##");
                // Since employees can't have inferiors, insted of 0, their field displays N/A, which reflects this reality
                if (personInFocus.Position == "Employee")
                {
                    referralBonusPay_textBox.Text = "N/A";
                }
                // All other worker categories can have inferiors, therefore their respective text field displays the actual value
                else
                {
                    referralBonusPay_textBox.Text = calculatedPay[2].ToString("0.##");
                }
                // The last text field is the sum of the three
                totalEarnings_textBox.Text = calculatedPay.Sum().ToString("0.##");
                UpdateWorkdays();
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Adjusts the interface to show information about selected Worker's inferior
        /// </summary>
        private void SwitchToInferiorView()
        {
            _WorkerView = false;
            _InferiorView = true;
            // Parses the selected inferior's ID
            var inferiorID = supervisedInferiors_comboBox.SelectedItem.ToString().Split(':')[0];
            // Gets the inferior Person object by ID and displays their info
            _Inferior = PersonFetcher.FetchPerson(int.Parse(inferiorID)).First();
            DisplayPersonInfo();
            HideFields();
        }

        /// <summary>
        /// Adjusts the interface to show information about logged-in Worker
        /// </summary>
        private void SwitchToSuperiorView()
        {
            _WorkerView = true;
            _InferiorView = false;
            DisplayPersonInfo();
            HideFields();
            supervisedInferiors_comboBox.Text = "Select...";
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void WorkerPayrollForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void multiPayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.WorkerID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Launches the calculation and displays hidden fields
        /// </summary>
        private void calculatePayroll_button_Click(object sender, EventArgs e)
        {
            if (_WorkerView = true && _InferiorView == false)
            {
                ActionLogger.LogAction($"calculated own payroll for the period between {startDate_dateTimePicker.Value.ToShortDateString()} and {endDate_dateTimePicker.Value.ToShortDateString()}", EntityIdHolder.WorkerID);
            }
            else
            {
                ActionLogger.LogAction($"calculated payroll for [inferior {_Inferior.MemberID}] for the period between {startDate_dateTimePicker.Value.ToShortDateString()} and {endDate_dateTimePicker.Value.ToShortDateString()}", EntityIdHolder.WorkerID);
            }
            ShowPayrollResults();
            ShowFields();
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void startDate_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateWorkdays();
            AdjustYearsWithCompany();
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void endDate_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateWorkdays();
            AdjustYearsWithCompany();
        }

        /// <summary>
        /// As soon as Inferior Worker is selected, the form starts displaying their info
        /// </summary>
        private void supervisedInferiors_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SwitchToInferiorView();
        }

        /// <summary>
        /// Calls the SwitchToSuperiorView method
        /// </summary>
        private void goBackToWorkerProfile_button_Click(object sender, EventArgs e)
        {
            SwitchToSuperiorView();
        }
    }
}
