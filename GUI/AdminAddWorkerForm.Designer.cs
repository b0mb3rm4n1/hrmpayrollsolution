﻿namespace GUI
{
    partial class AdminAddWorkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminAddWorkerForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singlePayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewWorker_label = new System.Windows.Forms.Label();
            this.addNewWorker_button = new System.Windows.Forms.Button();
            this.newWorkerID_textBox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newWorkerId_label = new System.Windows.Forms.Label();
            this.newWorkerFullName_label = new System.Windows.Forms.Label();
            this.newWorkerFullName_textBox = new System.Windows.Forms.TextBox();
            this.newWorkerDateOfEmployment_label = new System.Windows.Forms.Label();
            this.newWorkerDateOfEmployment_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.position_label = new System.Windows.Forms.Label();
            this.baseRate_label = new System.Windows.Forms.Label();
            this.baseRate_textBox = new System.Windows.Forms.TextBox();
            this.position_comboBox = new System.Windows.Forms.ComboBox();
            this.potentialSupervisors_comboBox = new System.Windows.Forms.ComboBox();
            this.assignToSupervisor_label = new System.Windows.Forms.Label();
            this.newUserLogin_label = new System.Windows.Forms.Label();
            this.newUserLogin_textBox = new System.Windows.Forms.TextBox();
            this.newUserPassword_textBox = new System.Windows.Forms.TextBox();
            this.newUserPassword_label = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.singlePayrollToolStripMenuItem,
            this.multiPayrollToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            this.goToMainMenuToolStripMenuItem.Click += new System.EventHandler(this.goToMainMenuToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // singlePayrollToolStripMenuItem
            // 
            this.singlePayrollToolStripMenuItem.Name = "singlePayrollToolStripMenuItem";
            this.singlePayrollToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.singlePayrollToolStripMenuItem.Text = "SinglePayroll";
            this.singlePayrollToolStripMenuItem.Click += new System.EventHandler(this.singlePayrollToolStripMenuItem_Click);
            // 
            // multiPayrollToolStripMenuItem
            // 
            this.multiPayrollToolStripMenuItem.Name = "multiPayrollToolStripMenuItem";
            this.multiPayrollToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.multiPayrollToolStripMenuItem.Text = "MultiPayroll";
            this.multiPayrollToolStripMenuItem.Click += new System.EventHandler(this.multiPayrollToolStripMenuItem_Click);
            // 
            // addNewWorker_label
            // 
            this.addNewWorker_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewWorker_label.AutoSize = true;
            this.addNewWorker_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewWorker_label.Location = new System.Drawing.Point(329, 34);
            this.addNewWorker_label.Name = "addNewWorker_label";
            this.addNewWorker_label.Size = new System.Drawing.Size(147, 19);
            this.addNewWorker_label.TabIndex = 2;
            this.addNewWorker_label.Text = "Add New Worker";
            // 
            // addNewWorker_button
            // 
            this.addNewWorker_button.Enabled = false;
            this.addNewWorker_button.Location = new System.Drawing.Point(328, 404);
            this.addNewWorker_button.Name = "addNewWorker_button";
            this.addNewWorker_button.Size = new System.Drawing.Size(148, 23);
            this.addNewWorker_button.TabIndex = 3;
            this.addNewWorker_button.Text = "Add New Worker";
            this.addNewWorker_button.UseVisualStyleBackColor = true;
            this.addNewWorker_button.Click += new System.EventHandler(this.addNewWorker_button_Click);
            // 
            // newWorkerID_textBox
            // 
            this.newWorkerID_textBox.Location = new System.Drawing.Point(408, 80);
            this.newWorkerID_textBox.Name = "newWorkerID_textBox";
            this.newWorkerID_textBox.ReadOnly = true;
            this.newWorkerID_textBox.Size = new System.Drawing.Size(200, 20);
            this.newWorkerID_textBox.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // newWorkerId_label
            // 
            this.newWorkerId_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newWorkerId_label.AutoSize = true;
            this.newWorkerId_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newWorkerId_label.Location = new System.Drawing.Point(216, 79);
            this.newWorkerId_label.Name = "newWorkerId_label";
            this.newWorkerId_label.Size = new System.Drawing.Size(138, 19);
            this.newWorkerId_label.TabIndex = 6;
            this.newWorkerId_label.Text = "New Worker ID:";
            // 
            // newWorkerFullName_label
            // 
            this.newWorkerFullName_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newWorkerFullName_label.AutoSize = true;
            this.newWorkerFullName_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newWorkerFullName_label.Location = new System.Drawing.Point(216, 118);
            this.newWorkerFullName_label.Name = "newWorkerFullName_label";
            this.newWorkerFullName_label.Size = new System.Drawing.Size(97, 19);
            this.newWorkerFullName_label.TabIndex = 8;
            this.newWorkerFullName_label.Text = "Full Name:";
            // 
            // newWorkerFullName_textBox
            // 
            this.newWorkerFullName_textBox.Location = new System.Drawing.Point(408, 117);
            this.newWorkerFullName_textBox.Name = "newWorkerFullName_textBox";
            this.newWorkerFullName_textBox.Size = new System.Drawing.Size(200, 20);
            this.newWorkerFullName_textBox.TabIndex = 7;
            // 
            // newWorkerDateOfEmployment_label
            // 
            this.newWorkerDateOfEmployment_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newWorkerDateOfEmployment_label.AutoSize = true;
            this.newWorkerDateOfEmployment_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newWorkerDateOfEmployment_label.Location = new System.Drawing.Point(216, 158);
            this.newWorkerDateOfEmployment_label.Name = "newWorkerDateOfEmployment_label";
            this.newWorkerDateOfEmployment_label.Size = new System.Drawing.Size(186, 19);
            this.newWorkerDateOfEmployment_label.TabIndex = 9;
            this.newWorkerDateOfEmployment_label.Text = "Date Of Employment:";
            // 
            // newWorkerDateOfEmployment_dateTimePicker
            // 
            this.newWorkerDateOfEmployment_dateTimePicker.CustomFormat = "";
            this.newWorkerDateOfEmployment_dateTimePicker.Location = new System.Drawing.Point(408, 156);
            this.newWorkerDateOfEmployment_dateTimePicker.MaxDate = new System.DateTime(2019, 5, 7, 0, 0, 0, 0);
            this.newWorkerDateOfEmployment_dateTimePicker.Name = "newWorkerDateOfEmployment_dateTimePicker";
            this.newWorkerDateOfEmployment_dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.newWorkerDateOfEmployment_dateTimePicker.TabIndex = 10;
            this.newWorkerDateOfEmployment_dateTimePicker.Value = new System.DateTime(2019, 5, 7, 0, 0, 0, 0);
            // 
            // position_label
            // 
            this.position_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.position_label.AutoSize = true;
            this.position_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position_label.Location = new System.Drawing.Point(216, 197);
            this.position_label.Name = "position_label";
            this.position_label.Size = new System.Drawing.Size(84, 19);
            this.position_label.TabIndex = 12;
            this.position_label.Text = "Position:";
            // 
            // baseRate_label
            // 
            this.baseRate_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseRate_label.AutoSize = true;
            this.baseRate_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseRate_label.Location = new System.Drawing.Point(216, 236);
            this.baseRate_label.Name = "baseRate_label";
            this.baseRate_label.Size = new System.Drawing.Size(156, 19);
            this.baseRate_label.TabIndex = 14;
            this.baseRate_label.Text = "Base Hourly Rate:";
            // 
            // baseRate_textBox
            // 
            this.baseRate_textBox.Location = new System.Drawing.Point(408, 235);
            this.baseRate_textBox.Name = "baseRate_textBox";
            this.baseRate_textBox.Size = new System.Drawing.Size(200, 20);
            this.baseRate_textBox.TabIndex = 13;
            // 
            // position_comboBox
            // 
            this.position_comboBox.FormattingEnabled = true;
            this.position_comboBox.Location = new System.Drawing.Point(408, 195);
            this.position_comboBox.Name = "position_comboBox";
            this.position_comboBox.Size = new System.Drawing.Size(200, 21);
            this.position_comboBox.TabIndex = 15;
            this.position_comboBox.Text = "Select...";
            this.position_comboBox.SelectedIndexChanged += new System.EventHandler(this.position_comboBox_SelectedIndexChanged);
            // 
            // potentialSupervisors_comboBox
            // 
            this.potentialSupervisors_comboBox.FormattingEnabled = true;
            this.potentialSupervisors_comboBox.Location = new System.Drawing.Point(408, 274);
            this.potentialSupervisors_comboBox.Name = "potentialSupervisors_comboBox";
            this.potentialSupervisors_comboBox.Size = new System.Drawing.Size(200, 21);
            this.potentialSupervisors_comboBox.TabIndex = 17;
            this.potentialSupervisors_comboBox.Text = "Select...";
            this.potentialSupervisors_comboBox.SelectedIndexChanged += new System.EventHandler(this.potentialSupervisors_comboBox_SelectedIndexChanged);
            // 
            // assignToSupervisor_label
            // 
            this.assignToSupervisor_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assignToSupervisor_label.AutoSize = true;
            this.assignToSupervisor_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignToSupervisor_label.Location = new System.Drawing.Point(216, 275);
            this.assignToSupervisor_label.Name = "assignToSupervisor_label";
            this.assignToSupervisor_label.Size = new System.Drawing.Size(188, 19);
            this.assignToSupervisor_label.TabIndex = 16;
            this.assignToSupervisor_label.Text = "Assign To Supervisor:";
            // 
            // newUserLogin_label
            // 
            this.newUserLogin_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newUserLogin_label.AutoSize = true;
            this.newUserLogin_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newUserLogin_label.Location = new System.Drawing.Point(218, 326);
            this.newUserLogin_label.Name = "newUserLogin_label";
            this.newUserLogin_label.Size = new System.Drawing.Size(142, 19);
            this.newUserLogin_label.TabIndex = 19;
            this.newUserLogin_label.Text = "New User Login:";
            // 
            // newUserLogin_textBox
            // 
            this.newUserLogin_textBox.Location = new System.Drawing.Point(408, 325);
            this.newUserLogin_textBox.Name = "newUserLogin_textBox";
            this.newUserLogin_textBox.Size = new System.Drawing.Size(200, 20);
            this.newUserLogin_textBox.TabIndex = 18;
            // 
            // newUserPassword_textBox
            // 
            this.newUserPassword_textBox.Location = new System.Drawing.Point(408, 362);
            this.newUserPassword_textBox.Name = "newUserPassword_textBox";
            this.newUserPassword_textBox.Size = new System.Drawing.Size(200, 20);
            this.newUserPassword_textBox.TabIndex = 18;
            // 
            // newUserPassword_label
            // 
            this.newUserPassword_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newUserPassword_label.AutoSize = true;
            this.newUserPassword_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newUserPassword_label.Location = new System.Drawing.Point(218, 363);
            this.newUserPassword_label.Name = "newUserPassword_label";
            this.newUserPassword_label.Size = new System.Drawing.Size(173, 19);
            this.newUserPassword_label.TabIndex = 19;
            this.newUserPassword_label.Text = "New User Password:";
            // 
            // AdminAddWorkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.newUserPassword_label);
            this.Controls.Add(this.newUserLogin_label);
            this.Controls.Add(this.newUserPassword_textBox);
            this.Controls.Add(this.newUserLogin_textBox);
            this.Controls.Add(this.potentialSupervisors_comboBox);
            this.Controls.Add(this.assignToSupervisor_label);
            this.Controls.Add(this.position_comboBox);
            this.Controls.Add(this.baseRate_label);
            this.Controls.Add(this.baseRate_textBox);
            this.Controls.Add(this.position_label);
            this.Controls.Add(this.newWorkerDateOfEmployment_dateTimePicker);
            this.Controls.Add(this.newWorkerDateOfEmployment_label);
            this.Controls.Add(this.newWorkerFullName_label);
            this.Controls.Add(this.newWorkerFullName_textBox);
            this.Controls.Add(this.newWorkerId_label);
            this.Controls.Add(this.newWorkerID_textBox);
            this.Controls.Add(this.addNewWorker_button);
            this.Controls.Add(this.addNewWorker_label);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AdminAddWorkerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin Add Worker";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminAddWorkerForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singlePayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPayrollToolStripMenuItem;
        private System.Windows.Forms.Label addNewWorker_label;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.Button addNewWorker_button;
        private System.Windows.Forms.TextBox newWorkerID_textBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label newWorkerId_label;
        private System.Windows.Forms.Label newWorkerFullName_label;
        private System.Windows.Forms.TextBox newWorkerFullName_textBox;
        private System.Windows.Forms.Label newWorkerDateOfEmployment_label;
        private System.Windows.Forms.DateTimePicker newWorkerDateOfEmployment_dateTimePicker;
        private System.Windows.Forms.Label position_label;
        private System.Windows.Forms.Label baseRate_label;
        private System.Windows.Forms.TextBox baseRate_textBox;
        private System.Windows.Forms.ComboBox position_comboBox;
        private System.Windows.Forms.ComboBox potentialSupervisors_comboBox;
        private System.Windows.Forms.Label assignToSupervisor_label;
        private System.Windows.Forms.Label newUserLogin_label;
        private System.Windows.Forms.TextBox newUserLogin_textBox;
        private System.Windows.Forms.TextBox newUserPassword_textBox;
        private System.Windows.Forms.Label newUserPassword_label;
    }
}