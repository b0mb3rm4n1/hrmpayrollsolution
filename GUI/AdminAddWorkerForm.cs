﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Data;
using Base;
using Service;

namespace GUI
{
    /// <summary>
    /// Form that allows Admin to add new Workers
    /// </summary>
    public partial class AdminAddWorkerForm : Form
    {
        /// <summary>_NewWorkerID is the ID that will be assigned to new Worker if such Worker is added</summary>
        private int _NewWorkerID { get; set; }
        /// <summary>_IsNameValid indicates if New Worker's name comprises only letters and white spaces</summary>
        private bool _IsNameValid { get; set; }
        /// <summary>_IsPositionSelected indicates if New Worker's Position is selected</summary>
        private bool _IsPositionSelected { get; set; }
        /// <summary>_IsBaseRateValid indicates if the appropriate text field contains a valid decimal number</summary>
        private bool _IsBaseRateValid { get; set; }
        /// <summary>_HasSuperior indicates if New Worker has Superior</summary>
        private bool _HasSuperior { get; set; }
        /// <summary>_PotentialSupervisors is a list of Persons who can be assigned to New Worker as Superior</summary>
        private List<Person> _PotentialSupervisors { get; set; }
        /// <summary>adminSinglePayrollForm is the only allowed instance of AdminAddWorkerForm</summary>
        private static AdminAddWorkerForm adminAddWorkerForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminAddWorkerForm instance
        /// </summary>
        /// <returns>The existing instance of AdminAddWorkerForm or a new AdminAddWorkerForm if such doesn't exist</returns>
        public static AdminAddWorkerForm GetAdminAddWorkerForm()
        {
            if (adminAddWorkerForm == null)
            {
                return new AdminAddWorkerForm();
            }
            else
            {
                return adminAddWorkerForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminSinglePayrollForm elements
        /// </summary>
        private AdminAddWorkerForm()
        {
            InitializeComponent();
            InitializeComponent2();
            DisplayNewWorkerId();
            DefinePositions();
            newWorkerDateOfEmployment_dateTimePicker.MaxDate = DateTime.Now;
        }

        /// <summary>
        /// Custom initializer, refreshes the form's UI elements
        /// </summary>
        public void InitializeComponent2()
        {
            _IsNameValid = true;
            _IsPositionSelected = true;
            _IsBaseRateValid = true;
            addNewWorker_button.Enabled = false;
            position_comboBox.Invalidate();
            newWorkerFullName_textBox.Text = "";
            baseRate_textBox.Text = "";
            position_comboBox.Text = "Select...";
            newUserLogin_textBox.Text = "";
            newUserPassword_textBox.Text = "";
            potentialSupervisors_comboBox.Items.Clear();
            potentialSupervisors_comboBox.Text = "Select...";
            newWorkerDateOfEmployment_dateTimePicker.Value = newWorkerDateOfEmployment_dateTimePicker.MaxDate;
        }

        /// <summary>
        /// Calls the ValidateNewWorkerInfo to determine whether the text fields contain valid values that can be assigned to New Worker
        /// Depending on the check result, either tells the AddNewWorker method to add that Worker or notifies User about the invalidity of the provided info
        /// </summary>
        private void AttemptToAddWorker()
        {
            try
            {
                if (ValidateNewWorkerInfo() == true)
                {
                    var response = PersonFetcher.AddNewWorker(_NewWorkerID, newWorkerFullName_textBox.Text, newWorkerDateOfEmployment_dateTimePicker.Value, position_comboBox.SelectedItem.ToString(), decimal.Parse(baseRate_textBox.Text));
                    if (response == true)
                    {
                        // If Superior is specified, they are assigned
                        if (_HasSuperior == true)
                        {
                            PersonFetcher.AssignInferiorToSuperior(_NewWorkerID, int.Parse(potentialSupervisors_comboBox.SelectedItem.ToString().Split(':')[0]));
                        }
                        // If no Superior is specified, none is assigned
                        else
                        {
                            PersonFetcher.AssignInferiorToSuperior(_NewWorkerID);
                        }
                        UserFetcher.AssignCredentialsToNewUser(_NewWorkerID, newUserLogin_textBox.Text, newUserPassword_textBox.Text);
                        MessageBox.Show("New worker added successfully!");
                        ActionLogger.LogAction($"successfully added new [worker {_NewWorkerID}] as [{position_comboBox.SelectedItem.ToString()}]", EntityIdHolder.AdminID);
                        // The UI is refreshed and the next assignable unique Person ID is shown
                        InitializeComponent2();
                        DisplayNewWorkerId();
                    }
                    else
                    {
                        ActionLogger.LogAction($"failed to add a new worker as [{position_comboBox.SelectedItem.ToString()}]", EntityIdHolder.AdminID);
                        MessageBox.Show("Worker was not added!");
                    }
                }
                else
                {
                    ActionLogger.LogAction($"failed to add a new worker as [{position_comboBox.SelectedItem.ToString()}]", EntityIdHolder.AdminID);
                    MessageBox.Show("One or more values are incorrect!");
                    // The values are presumed to be true, the validating method overrides them if necessary
                    _IsBaseRateValid = true;
                    _IsNameValid = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Displays the next valid ID that will be assigned to newly created Person
        /// </summary>
        private void DisplayNewWorkerId()
        {
            // Fetches the maximum Person ID contained in the DB table, increments it by one and displays it
            _NewWorkerID = PersonFetcher.FetchMaxId().First() + 1;
            newWorkerID_textBox.Text = _NewWorkerID.ToString();
        }

        /// <summary>
        /// Checks the entered information about New Worker against a set of requirements
        /// </summary>
        /// <returns>A boolean indicating if the entered information is valid</returns>
        public bool ValidateNewWorkerInfo()
        {
            // To make things simple, the fields just have to contain at least something
            if (newWorkerFullName_textBox.Text.Count() > 0 && baseRate_textBox.Text.Count() > 0 && newUserLogin_textBox.Text.Count() > 0 && newUserPassword_textBox.Text.Count() > 0)
            {
                // Iterates over each character contained in the Name text field
                foreach (var letter in newWorkerFullName_textBox.Text)
                {
                    // Only letters and white spaces are allowed as Name
                    if (!char.IsLetter(letter) && !char.IsWhiteSpace(letter))
                    {
                        _IsNameValid = false;
                        break;
                    }
                }

                // Iterates over each character contained in the Base Rate text field
                var decimalPoint = default(int);
                foreach (var digit in baseRate_textBox.Text)
                {
                    // Only didits and one decimal point are allowed as Base Rate
                    if (digit == '.') { decimalPoint++; }
                    if ((!char.IsDigit(digit) && digit != '.') || decimalPoint > 1)
                    {
                        _IsBaseRateValid = false;
                        break;
                    }
                }
            }
            else
            {
                return false;
            }

            // If everything fits the bill, we proceed
            if (_IsNameValid == true && _IsPositionSelected == true && _IsBaseRateValid == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Fetches and displays all Worker Positions mentioned in the DB
        /// </summary>
        public void DefinePositions()
        {
            try
            {
                // Iterates over each position in the list and adds it to the displayed list
                foreach (var position in PersonFetcher.FetchPositions())
                {
                    position_comboBox.Items.Add(position);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Rearranges the list of potential Persons that the new Worker can be assigned to as inferior depending on Worker's selected Position
        /// </summary>
        /// <param name="newWorkerPosition">New Worker's position</param>
        private void ShowPotentialSupervisors(string newWorkerPosition)
        {
            try
            {
                if (newWorkerPosition == "Employee")
                {
                    // Employees can be supervised by Managers and Salesmen
                    _PotentialSupervisors = PersonFetcher.FetchPotentialSupervisors("'Manager' OR Position = 'Salesman' ORDER BY Position");
                    foreach (var potentialSupervisor in _PotentialSupervisors)
                    {
                        potentialSupervisors_comboBox.Items.Add($"{potentialSupervisor.MemberID}: {potentialSupervisor.FullName} - {potentialSupervisor.Position}");
                    }
                }
                else if (newWorkerPosition == "Manager")
                {
                    // Managers can be supervised by Salesmen
                    _PotentialSupervisors = PersonFetcher.FetchPotentialSupervisors("'Salesman'");
                    foreach (var potentialSupervisor in _PotentialSupervisors)
                    {
                        potentialSupervisors_comboBox.Items.Add($"{potentialSupervisor.MemberID}: {potentialSupervisor.FullName} - {potentialSupervisor.Position}");
                    }
                }
                else if (newWorkerPosition == "Salesman")
                {
                    // Salesmen are the highest position, therefore no superiors for them
                    potentialSupervisors_comboBox.Items.Clear();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void goToMainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminPanelForm.GetAdminPanelForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Once Admin selects Supervisor, the property is adjusted to reflect the change
        /// </summary>
        private void potentialSupervisors_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            _HasSuperior = true;
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void AdminAddWorkerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.AdminID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void singlePayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminSinglePayrollForm.GetAdminSinglePayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void multiPayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Calls the AttemptToAddWorker method
        /// </summary>
        private void addNewWorker_button_Click(object sender, EventArgs e)
        {
            AttemptToAddWorker();
        }

        /// <summary>
        /// Every time Admin selects New Worker's Position or changes it, the list of potential supervisors gets redefined and the button becomes available
        /// </summary>
        private void position_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            potentialSupervisors_comboBox.Items.Clear();
            ShowPotentialSupervisors(position_comboBox.SelectedItem.ToString());
            addNewWorker_button.Enabled = true;
        }
    }
}
