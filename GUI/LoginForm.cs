﻿using System;
using System.Linq;
using System.Windows.Forms;
using Data;
using Service;

namespace GUI
{
    /// <summary>
    /// Form that prompts User to enter login and password
    /// </summary>
    public partial class LoginForm : Form
    {
        /// <summary>IsAuthorized is an indicator that determines if User is allowed to proceed further</summary>
        private bool IsAuthorized { get; set; }
        /// <summary>loginForm is the only allowed instance of LoginForm</summary>
        private static LoginForm loginForm { get; set; }

        /// <summary>
        /// Fetches or creates LoginForm instance
        /// </summary>
        /// <returns>The existing instance of LoginForm or a new LoginForm if such doesn't exist</returns>
        public static LoginForm GetLoginForm()
        {
            if (loginForm == null)
            {
                return new LoginForm();
            }
            else
            {
                return loginForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of LoginForm elements
        /// </summary>
        private LoginForm()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        /// <summary>
        /// Custom initializer, refreshes the form's UI elements
        /// </summary>
        private void InitializeComponent2()
        {
            login_textBox.Text = "Login";
            password_textBox.Text = "Login";
            login_textBox.Focus();
            login_textBox.SelectAll();
        }

        /// <summary>
        /// Takes the information from the login and password textboxes and checks if there is a match in the DB
        /// </summary>
        private void AttemptToLogin()
        {
            // Sends the info to the CheckUserCredentials and receives a response
            var userCredentialsCheckResult = UserFetcher.CheckUserCredentials(login_textBox.Text, password_textBox.Text);

            // if CheckUserCredentials doesn't return an empty list, the user proceeds onto the next form
            if (userCredentialsCheckResult.Any())
            {
                // The utility EntityIdHolder class remembers the ID of the user that entered the valid login-password pair, so that the program knows whose information to show later
                EntityIdHolder.WorkerID = userCredentialsCheckResult.First().UserID;
                // the Login form goes away as it is no longer needed, but we can't dispose it since it is the entry form of the application
                this.Hide();
                // and depending on the user's ID (0 is the default value of the property, if it's unchanged means that it was Admin who logged in) the appropriate form shows up
                if (EntityIdHolder.WorkerID == EntityIdHolder.AdminID)
                {
                     var that = AdminPanelForm.GetAdminPanelForm();
                     that.Show();
                }
                else
                {
                     var that = WorkerPayrollForm.GetWorkerPayrollForm();
                     that.Show();
                }
                // The property is set to true to prevent the following turn of events
                IsAuthorized = true;
                ActionLogger.LogAction("successful login", EntityIdHolder.WorkerID);
            }

            // If User wasn't authorized in the previous block
            if (IsAuthorized == false)
            {
                // User sees that the info they entered is incorrect, and the fields are reset
                MessageBox.Show("Incorrect login or password");
                InitializeComponent2();
                ActionLogger.LogAction("failed login attempt", EntityIdHolder.WorkerID);
            }
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ActionLogger.LogAction("exited the application", EntityIdHolder.WorkerID);
            Application.Exit();
        }

        /// <summary>
        /// Allows User to press Enter Key to initiate authorization
        /// </summary>
        private void password_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AttemptToLogin();
            }
        }

        /// <summary>
        /// Allows User to press Enter Key to initiate authorization
        /// </summary>
        private void login_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AttemptToLogin();
            }
        }

        /// <summary>
        /// Calls the AttemptToLogin method
        /// </summary>
        private void login_button_Click(object sender, EventArgs e)
        {
            AttemptToLogin();
        }
    }
}
