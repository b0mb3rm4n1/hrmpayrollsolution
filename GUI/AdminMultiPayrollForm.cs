﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Base;
using Data;
using Core;
using Service;

namespace GUI
{
    /// <summary>
    /// Form that displays all staff and their respective pay for the specified period
    /// </summary>
    public partial class AdminMultiPayrollForm : Form
    {
        /// <summary>_Persons is a list of all company staff</summary>
        private static List<Person> _Persons { get; set; }
        /// <summary>_CurrentPerson is the currently selected Person object</summary>
        private Person _CurrentPerson { get; set; }
        /// <summary>_AggregatePay is an array containing separate amounts of base pay, yearly bonus pay, and referral pay of all Workers</summary>
        private decimal[] _AggregatePay { get; set; }
        /// <summary>adminMultiPayrollForm is the only allowed instance of AdminMultiPayrollForm</summary>
        private static AdminMultiPayrollForm adminMultiPayrollForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminMultiPayrollForm instance
        /// </summary>
        /// <returns>The existing instance of AdminMultiPayrollForm or a new AdminMultiPayrollForm if such doesn't exist</returns>
        public static AdminMultiPayrollForm GetAdminMultiPayrollForm()
        {
            if (adminMultiPayrollForm == null)
            {
                return new AdminMultiPayrollForm();
            }
            else
            {
                return adminMultiPayrollForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminSinglePayrollForm elements and fills the _Persons list with objects
        /// </summary>
        private AdminMultiPayrollForm()
        {
            InitializeComponent();
            startDate_dateTimePicker.MaxDate = DateTime.Now;
            endDate_dateTimePicker.MaxDate = DateTime.Now;
            _Persons = PersonFetcher.FetchAll();
        }

        /// <summary>
        /// Updates the UI and shows hidden elements 
        /// </summary>
        private void GetStaff()
        {
            allStaff_gridView.Rows.Clear();
            LoadPersons();
            startDate_dateTimePicker.Enabled = true;
            endDate_dateTimePicker.Enabled = true;
            calculatePayroll_button.Enabled = true;
            totalWorkdays_label.Visible = true;
            totalWorkdays_textBox.Visible = true;
            UpdateWorkdays();
        }

        /// <summary>
        /// Prints information about each person in the GridView area of the screen
        /// </summary>
        private void LoadPersons()
        {
            try
            {
                for (var i = 0; i < _Persons.Count; i++)
                {
                    string[] person = new string[5];
                    person[0] = _Persons[i].MemberID.ToString();
                    person[1] = _Persons[i].FullName;
                    person[2] = _Persons[i].DateOfEmployment.ToShortDateString();
                    person[3] = _Persons[i].Position;
                    person[4] = _Persons[i].BaseHourlyRate.ToString();
                    allStaff_gridView.Rows.Add(person);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Shows payroll results for each and all Persons at once in the appropriate fields
        /// </summary>
        private void ShowPayrollResultsForAll()
        {
            // Data is refreshed each time the button is pressed
            _AggregatePay = new decimal[3]; 
            // If the list contains at least one Person object, the method proceeds
            if (_Persons.Any())
            {
                for (var i = 0; i < _Persons.Count; i++)
                {
                    // Casts each one into a derived class depending on their Position
                    var person = _Persons[i];
                    _CurrentPerson = ClassConverter.ConvertBaseToDerived(person);
                    // Calculates and retrieves pay amount for each Person, adds it to their entry in the table
                    decimal[] personsCalculatedPay = _CurrentPerson.CalculatePay(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value);
                    allStaff_gridView.Rows[i].Cells[5].Value = personsCalculatedPay.Sum().ToString("0.##");
                    // Increments the respective parts of the form's property
                    _AggregatePay[0] += personsCalculatedPay[0];
                    _AggregatePay[1] += personsCalculatedPay[1];
                    _AggregatePay[2] += personsCalculatedPay[2];
                }
                // Updates the appropriate fields with the accumulated numbers 
                totalBasePay_textBox.Text = _AggregatePay[0].ToString("0.##");
                yearBonuses_textBox.Text = _AggregatePay[1].ToString("0.##");
                referralBonuses_textBox.Text = _AggregatePay[2].ToString("0.##");
                totalEarnings_textBox.Text = _AggregatePay.Sum().ToString("0.##");
                // Making the fields visible
                totalBasePay_label.Visible = true;
                totalBasePay_textBox.Visible = true;
                yearBonus_label.Visible = true;
                yearBonuses_textBox.Visible = true;
                referralBonusPay_label.Visible = true;
                referralBonuses_textBox.Visible = true;
                totalEarnings_label.Visible = true;
                totalEarnings_textBox.Visible = true;
            }
        }

        /// <summary>
        /// Refreshes certain UI elements depending on context
        /// </summary>
        private void UpdateWorkdays()
        {
            // If the values are not in order, they are reset, and the text field doesn't change
            if (startDate_dateTimePicker.Value > endDate_dateTimePicker.Value)
            {
                ResetCalendars();
            }
            // If the date range is correct, the text field reflects the updated number of workdays, received from the method
            else
            {
                totalWorkdays_textBox.Text = WorkdayCounter.CountWorkdays(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value).ToString(); 
            }
        }

        /// <summary>
        /// Resets the values currently held by the DateTimePicker controls
        /// </summary>
        private void ResetCalendars()
        {
            startDate_dateTimePicker.Value = startDate_dateTimePicker.MaxDate;
            endDate_dateTimePicker.Value = endDate_dateTimePicker.MaxDate;
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void startDate_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateWorkdays();
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void endDate_dateTimePicker_ValueChanged_1(object sender, EventArgs e)
        {
            UpdateWorkdays();
        }

        /// <summary>
        /// Refreshes the interface
        /// </summary>
        private void clearSelection_button_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            InitializeComponent();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void goToMainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminPanelForm.GetAdminPanelForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.AdminID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Calls the GetStaff method
        /// </summary>
        private void getAllStaff_button_Click(object sender, EventArgs e)
        {
            GetStaff();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void MultiPayrollForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void switchToSinglePayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminSinglePayrollForm.GetAdminSinglePayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Calls the ShowPayrollResultsForAll method
        /// </summary>
        private void calculatePayroll_button_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction($"calculated payroll for all staff for the period between {startDate_dateTimePicker.Value.ToShortDateString()} and {endDate_dateTimePicker.Value.ToShortDateString()}", EntityIdHolder.AdminID);
            ShowPayrollResultsForAll();
        }
    }
}
