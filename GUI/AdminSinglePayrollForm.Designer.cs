﻿namespace GUI
{
    partial class AdminSinglePayrollForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminSinglePayrollForm));
            this.selectPerson_label = new System.Windows.Forms.Label();
            this.personSelection_comboBox = new System.Windows.Forms.ComboBox();
            this.clearSelection_button = new System.Windows.Forms.Button();
            this.memberID_label = new System.Windows.Forms.Label();
            this.fullName_label = new System.Windows.Forms.Label();
            this.dateOfEmployment_label = new System.Windows.Forms.Label();
            this.position_label = new System.Windows.Forms.Label();
            this.baseRate_label = new System.Windows.Forms.Label();
            this.yearsWithCompany_label = new System.Windows.Forms.Label();
            this.supervises_label = new System.Windows.Forms.Label();
            this.memberID_textBox = new System.Windows.Forms.TextBox();
            this.fullName_textBox = new System.Windows.Forms.TextBox();
            this.dateOfEmployment_textBox = new System.Windows.Forms.TextBox();
            this.position_textBox = new System.Windows.Forms.TextBox();
            this.baseRate_textBox = new System.Windows.Forms.TextBox();
            this.yearsWithCompany_textBox = new System.Windows.Forms.TextBox();
            this.numberOfInferiors_textBox = new System.Windows.Forms.TextBox();
            this.payrollStart_label = new System.Windows.Forms.Label();
            this.payrollEnd_label = new System.Windows.Forms.Label();
            this.startDate_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.endDate_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.calculatePayroll_button = new System.Windows.Forms.Button();
            this.totalWorkdays_label = new System.Windows.Forms.Label();
            this.totalEarnings_label = new System.Windows.Forms.Label();
            this.totalWorkdays_textBox = new System.Windows.Forms.TextBox();
            this.totalEarnings_textBox = new System.Windows.Forms.TextBox();
            this.displayInferiors_button = new System.Windows.Forms.Button();
            this.basePay_label = new System.Windows.Forms.Label();
            this.basePay_textBox = new System.Windows.Forms.TextBox();
            this.yearBonus_textBox = new System.Windows.Forms.TextBox();
            this.yearBonus_label = new System.Windows.Forms.Label();
            this.referralBonusPay_textBox = new System.Windows.Forms.TextBox();
            this.referralBonusPay_label = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchToMultiPayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectPerson_label
            // 
            this.selectPerson_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPerson_label.AutoSize = true;
            this.selectPerson_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectPerson_label.Location = new System.Drawing.Point(43, 45);
            this.selectPerson_label.Name = "selectPerson_label";
            this.selectPerson_label.Size = new System.Drawing.Size(126, 19);
            this.selectPerson_label.TabIndex = 0;
            this.selectPerson_label.Text = "Select Person:";
            // 
            // personSelection_comboBox
            // 
            this.personSelection_comboBox.FormattingEnabled = true;
            this.personSelection_comboBox.Location = new System.Drawing.Point(239, 45);
            this.personSelection_comboBox.Name = "personSelection_comboBox";
            this.personSelection_comboBox.Size = new System.Drawing.Size(176, 21);
            this.personSelection_comboBox.TabIndex = 1;
            this.personSelection_comboBox.Text = "Select...";
            this.personSelection_comboBox.SelectedIndexChanged += new System.EventHandler(this.personSelection_comboBox_SelectedIndexChanged);
            // 
            // clearSelection_button
            // 
            this.clearSelection_button.BackColor = System.Drawing.Color.Lime;
            this.clearSelection_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearSelection_button.Location = new System.Drawing.Point(418, 45);
            this.clearSelection_button.Name = "clearSelection_button";
            this.clearSelection_button.Size = new System.Drawing.Size(57, 22);
            this.clearSelection_button.TabIndex = 2;
            this.clearSelection_button.Text = "Clear";
            this.clearSelection_button.UseVisualStyleBackColor = false;
            this.clearSelection_button.Click += new System.EventHandler(this.clearSelection_button_Click);
            // 
            // memberID_label
            // 
            this.memberID_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memberID_label.AutoSize = true;
            this.memberID_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberID_label.Location = new System.Drawing.Point(39, 116);
            this.memberID_label.Name = "memberID_label";
            this.memberID_label.Size = new System.Drawing.Size(105, 19);
            this.memberID_label.TabIndex = 3;
            this.memberID_label.Text = "Member ID:";
            // 
            // fullName_label
            // 
            this.fullName_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fullName_label.AutoSize = true;
            this.fullName_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullName_label.Location = new System.Drawing.Point(39, 158);
            this.fullName_label.Name = "fullName_label";
            this.fullName_label.Size = new System.Drawing.Size(97, 19);
            this.fullName_label.TabIndex = 4;
            this.fullName_label.Text = "Full Name:";
            // 
            // dateOfEmployment_label
            // 
            this.dateOfEmployment_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateOfEmployment_label.AutoSize = true;
            this.dateOfEmployment_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateOfEmployment_label.Location = new System.Drawing.Point(39, 201);
            this.dateOfEmployment_label.Name = "dateOfEmployment_label";
            this.dateOfEmployment_label.Size = new System.Drawing.Size(186, 19);
            this.dateOfEmployment_label.TabIndex = 5;
            this.dateOfEmployment_label.Text = "Date Of Employment:";
            // 
            // position_label
            // 
            this.position_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.position_label.AutoSize = true;
            this.position_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position_label.Location = new System.Drawing.Point(39, 244);
            this.position_label.Name = "position_label";
            this.position_label.Size = new System.Drawing.Size(84, 19);
            this.position_label.TabIndex = 6;
            this.position_label.Text = "Position:";
            // 
            // baseRate_label
            // 
            this.baseRate_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseRate_label.AutoSize = true;
            this.baseRate_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseRate_label.Location = new System.Drawing.Point(39, 280);
            this.baseRate_label.Name = "baseRate_label";
            this.baseRate_label.Size = new System.Drawing.Size(156, 19);
            this.baseRate_label.TabIndex = 7;
            this.baseRate_label.Text = "Base Hourly Rate:";
            // 
            // yearsWithCompany_label
            // 
            this.yearsWithCompany_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yearsWithCompany_label.AutoSize = true;
            this.yearsWithCompany_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearsWithCompany_label.Location = new System.Drawing.Point(39, 349);
            this.yearsWithCompany_label.Name = "yearsWithCompany_label";
            this.yearsWithCompany_label.Size = new System.Drawing.Size(127, 19);
            this.yearsWithCompany_label.TabIndex = 8;
            this.yearsWithCompany_label.Text = "Years By Date:";
            // 
            // supervises_label
            // 
            this.supervises_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supervises_label.AutoSize = true;
            this.supervises_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervises_label.Location = new System.Drawing.Point(39, 393);
            this.supervises_label.Name = "supervises_label";
            this.supervises_label.Size = new System.Drawing.Size(103, 19);
            this.supervises_label.TabIndex = 9;
            this.supervises_label.Text = "Supervises:";
            this.supervises_label.Visible = false;
            // 
            // memberID_textBox
            // 
            this.memberID_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberID_textBox.Location = new System.Drawing.Point(239, 116);
            this.memberID_textBox.Name = "memberID_textBox";
            this.memberID_textBox.ReadOnly = true;
            this.memberID_textBox.Size = new System.Drawing.Size(176, 23);
            this.memberID_textBox.TabIndex = 10;
            // 
            // fullName_textBox
            // 
            this.fullName_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullName_textBox.Location = new System.Drawing.Point(239, 157);
            this.fullName_textBox.Name = "fullName_textBox";
            this.fullName_textBox.ReadOnly = true;
            this.fullName_textBox.Size = new System.Drawing.Size(176, 23);
            this.fullName_textBox.TabIndex = 11;
            // 
            // dateOfEmployment_textBox
            // 
            this.dateOfEmployment_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateOfEmployment_textBox.Location = new System.Drawing.Point(239, 197);
            this.dateOfEmployment_textBox.Name = "dateOfEmployment_textBox";
            this.dateOfEmployment_textBox.ReadOnly = true;
            this.dateOfEmployment_textBox.Size = new System.Drawing.Size(176, 23);
            this.dateOfEmployment_textBox.TabIndex = 12;
            // 
            // position_textBox
            // 
            this.position_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position_textBox.Location = new System.Drawing.Point(239, 240);
            this.position_textBox.Name = "position_textBox";
            this.position_textBox.ReadOnly = true;
            this.position_textBox.Size = new System.Drawing.Size(176, 23);
            this.position_textBox.TabIndex = 13;
            // 
            // baseRate_textBox
            // 
            this.baseRate_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseRate_textBox.Location = new System.Drawing.Point(239, 280);
            this.baseRate_textBox.Name = "baseRate_textBox";
            this.baseRate_textBox.ReadOnly = true;
            this.baseRate_textBox.Size = new System.Drawing.Size(176, 23);
            this.baseRate_textBox.TabIndex = 14;
            // 
            // yearsWithCompany_textBox
            // 
            this.yearsWithCompany_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearsWithCompany_textBox.Location = new System.Drawing.Point(239, 345);
            this.yearsWithCompany_textBox.Name = "yearsWithCompany_textBox";
            this.yearsWithCompany_textBox.ReadOnly = true;
            this.yearsWithCompany_textBox.Size = new System.Drawing.Size(176, 23);
            this.yearsWithCompany_textBox.TabIndex = 15;
            // 
            // numberOfInferiors_textBox
            // 
            this.numberOfInferiors_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfInferiors_textBox.Location = new System.Drawing.Point(239, 390);
            this.numberOfInferiors_textBox.Name = "numberOfInferiors_textBox";
            this.numberOfInferiors_textBox.ReadOnly = true;
            this.numberOfInferiors_textBox.Size = new System.Drawing.Size(176, 23);
            this.numberOfInferiors_textBox.TabIndex = 16;
            this.numberOfInferiors_textBox.Visible = false;
            // 
            // payrollStart_label
            // 
            this.payrollStart_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.payrollStart_label.AutoSize = true;
            this.payrollStart_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollStart_label.Location = new System.Drawing.Point(523, 44);
            this.payrollStart_label.Name = "payrollStart_label";
            this.payrollStart_label.Size = new System.Drawing.Size(226, 19);
            this.payrollStart_label.TabIndex = 17;
            this.payrollStart_label.Text = "Specify Payroll Start Date:";
            // 
            // payrollEnd_label
            // 
            this.payrollEnd_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.payrollEnd_label.AutoSize = true;
            this.payrollEnd_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollEnd_label.Location = new System.Drawing.Point(532, 90);
            this.payrollEnd_label.Name = "payrollEnd_label";
            this.payrollEnd_label.Size = new System.Drawing.Size(218, 19);
            this.payrollEnd_label.TabIndex = 18;
            this.payrollEnd_label.Text = "Specify Payroll End Date:";
            // 
            // startDate_dateTimePicker
            // 
            this.startDate_dateTimePicker.Location = new System.Drawing.Point(536, 66);
            this.startDate_dateTimePicker.Name = "startDate_dateTimePicker";
            this.startDate_dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDate_dateTimePicker.TabIndex = 19;
            this.startDate_dateTimePicker.ValueChanged += new System.EventHandler(this.startDate_dateTimePicker_ValueChanged);
            // 
            // endDate_dateTimePicker
            // 
            this.endDate_dateTimePicker.Location = new System.Drawing.Point(536, 112);
            this.endDate_dateTimePicker.Name = "endDate_dateTimePicker";
            this.endDate_dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDate_dateTimePicker.TabIndex = 20;
            this.endDate_dateTimePicker.ValueChanged += new System.EventHandler(this.endDate_dateTimePicker_ValueChanged);
            // 
            // calculatePayroll_button
            // 
            this.calculatePayroll_button.Enabled = false;
            this.calculatePayroll_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculatePayroll_button.Location = new System.Drawing.Point(536, 138);
            this.calculatePayroll_button.Name = "calculatePayroll_button";
            this.calculatePayroll_button.Size = new System.Drawing.Size(200, 29);
            this.calculatePayroll_button.TabIndex = 21;
            this.calculatePayroll_button.Text = "Calculate Payroll";
            this.calculatePayroll_button.UseVisualStyleBackColor = true;
            this.calculatePayroll_button.Click += new System.EventHandler(this.calculatePayroll_button_Click);
            // 
            // totalWorkdays_label
            // 
            this.totalWorkdays_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalWorkdays_label.AutoSize = true;
            this.totalWorkdays_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalWorkdays_label.Location = new System.Drawing.Point(529, 181);
            this.totalWorkdays_label.Name = "totalWorkdays_label";
            this.totalWorkdays_label.Size = new System.Drawing.Size(224, 19);
            this.totalWorkdays_label.TabIndex = 22;
            this.totalWorkdays_label.Text = "Total Workdays In Period:";
            this.totalWorkdays_label.Visible = false;
            // 
            // totalEarnings_label
            // 
            this.totalEarnings_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalEarnings_label.AutoSize = true;
            this.totalEarnings_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalEarnings_label.Location = new System.Drawing.Point(529, 368);
            this.totalEarnings_label.Name = "totalEarnings_label";
            this.totalEarnings_label.Size = new System.Drawing.Size(226, 19);
            this.totalEarnings_label.TabIndex = 23;
            this.totalEarnings_label.Text = "Total Earnings For Period:";
            this.totalEarnings_label.Visible = false;
            // 
            // totalWorkdays_textBox
            // 
            this.totalWorkdays_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalWorkdays_textBox.Location = new System.Drawing.Point(533, 203);
            this.totalWorkdays_textBox.Name = "totalWorkdays_textBox";
            this.totalWorkdays_textBox.ReadOnly = true;
            this.totalWorkdays_textBox.Size = new System.Drawing.Size(214, 23);
            this.totalWorkdays_textBox.TabIndex = 24;
            this.totalWorkdays_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalWorkdays_textBox.Visible = false;
            // 
            // totalEarnings_textBox
            // 
            this.totalEarnings_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalEarnings_textBox.Location = new System.Drawing.Point(533, 390);
            this.totalEarnings_textBox.Name = "totalEarnings_textBox";
            this.totalEarnings_textBox.ReadOnly = true;
            this.totalEarnings_textBox.Size = new System.Drawing.Size(214, 23);
            this.totalEarnings_textBox.TabIndex = 25;
            this.totalEarnings_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalEarnings_textBox.Visible = false;
            // 
            // displayInferiors_button
            // 
            this.displayInferiors_button.BackColor = System.Drawing.Color.DarkGray;
            this.displayInferiors_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayInferiors_button.Location = new System.Drawing.Point(420, 390);
            this.displayInferiors_button.Name = "displayInferiors_button";
            this.displayInferiors_button.Size = new System.Drawing.Size(55, 23);
            this.displayInferiors_button.TabIndex = 29;
            this.displayInferiors_button.Text = "See";
            this.displayInferiors_button.UseVisualStyleBackColor = false;
            this.displayInferiors_button.Visible = false;
            // 
            // basePay_label
            // 
            this.basePay_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.basePay_label.AutoSize = true;
            this.basePay_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basePay_label.Location = new System.Drawing.Point(593, 226);
            this.basePay_label.Name = "basePay_label";
            this.basePay_label.Size = new System.Drawing.Size(88, 19);
            this.basePay_label.TabIndex = 30;
            this.basePay_label.Text = "Base Pay:";
            this.basePay_label.Visible = false;
            // 
            // basePay_textBox
            // 
            this.basePay_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basePay_textBox.Location = new System.Drawing.Point(532, 248);
            this.basePay_textBox.Name = "basePay_textBox";
            this.basePay_textBox.ReadOnly = true;
            this.basePay_textBox.Size = new System.Drawing.Size(214, 23);
            this.basePay_textBox.TabIndex = 31;
            this.basePay_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.basePay_textBox.Visible = false;
            // 
            // yearBonus_textBox
            // 
            this.yearBonus_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearBonus_textBox.Location = new System.Drawing.Point(532, 294);
            this.yearBonus_textBox.Name = "yearBonus_textBox";
            this.yearBonus_textBox.ReadOnly = true;
            this.yearBonus_textBox.Size = new System.Drawing.Size(214, 23);
            this.yearBonus_textBox.TabIndex = 33;
            this.yearBonus_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yearBonus_textBox.Visible = false;
            // 
            // yearBonus_label
            // 
            this.yearBonus_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yearBonus_label.AutoSize = true;
            this.yearBonus_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearBonus_label.Location = new System.Drawing.Point(560, 272);
            this.yearBonus_label.Name = "yearBonus_label";
            this.yearBonus_label.Size = new System.Drawing.Size(159, 19);
            this.yearBonus_label.TabIndex = 32;
            this.yearBonus_label.Text = "Year-based Bonus:";
            this.yearBonus_label.Visible = false;
            // 
            // referralBonusPay_textBox
            // 
            this.referralBonusPay_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referralBonusPay_textBox.Location = new System.Drawing.Point(532, 342);
            this.referralBonusPay_textBox.Name = "referralBonusPay_textBox";
            this.referralBonusPay_textBox.ReadOnly = true;
            this.referralBonusPay_textBox.Size = new System.Drawing.Size(214, 23);
            this.referralBonusPay_textBox.TabIndex = 35;
            this.referralBonusPay_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.referralBonusPay_textBox.Visible = false;
            // 
            // referralBonusPay_label
            // 
            this.referralBonusPay_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.referralBonusPay_label.AutoSize = true;
            this.referralBonusPay_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referralBonusPay_label.Location = new System.Drawing.Point(575, 320);
            this.referralBonusPay_label.Name = "referralBonusPay_label";
            this.referralBonusPay_label.Size = new System.Drawing.Size(136, 19);
            this.referralBonusPay_label.TabIndex = 34;
            this.referralBonusPay_label.Text = "Referral Bonus:";
            this.referralBonusPay_label.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.switchToMultiPayrollToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 37;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            this.goToMainMenuToolStripMenuItem.Click += new System.EventHandler(this.goToMainMenuToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // switchToMultiPayrollToolStripMenuItem
            // 
            this.switchToMultiPayrollToolStripMenuItem.Name = "switchToMultiPayrollToolStripMenuItem";
            this.switchToMultiPayrollToolStripMenuItem.Size = new System.Drawing.Size(137, 20);
            this.switchToMultiPayrollToolStripMenuItem.Text = "Switch To MultiPayroll";
            this.switchToMultiPayrollToolStripMenuItem.Click += new System.EventHandler(this.switchToMultiPayrollToolStripMenuItem_Click);
            // 
            // SinglePayrollForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.referralBonusPay_textBox);
            this.Controls.Add(this.referralBonusPay_label);
            this.Controls.Add(this.yearBonus_textBox);
            this.Controls.Add(this.yearBonus_label);
            this.Controls.Add(this.basePay_textBox);
            this.Controls.Add(this.basePay_label);
            this.Controls.Add(this.displayInferiors_button);
            this.Controls.Add(this.totalEarnings_textBox);
            this.Controls.Add(this.totalWorkdays_textBox);
            this.Controls.Add(this.totalEarnings_label);
            this.Controls.Add(this.totalWorkdays_label);
            this.Controls.Add(this.calculatePayroll_button);
            this.Controls.Add(this.endDate_dateTimePicker);
            this.Controls.Add(this.startDate_dateTimePicker);
            this.Controls.Add(this.payrollEnd_label);
            this.Controls.Add(this.payrollStart_label);
            this.Controls.Add(this.numberOfInferiors_textBox);
            this.Controls.Add(this.yearsWithCompany_textBox);
            this.Controls.Add(this.baseRate_textBox);
            this.Controls.Add(this.position_textBox);
            this.Controls.Add(this.dateOfEmployment_textBox);
            this.Controls.Add(this.fullName_textBox);
            this.Controls.Add(this.memberID_textBox);
            this.Controls.Add(this.supervises_label);
            this.Controls.Add(this.yearsWithCompany_label);
            this.Controls.Add(this.baseRate_label);
            this.Controls.Add(this.position_label);
            this.Controls.Add(this.dateOfEmployment_label);
            this.Controls.Add(this.fullName_label);
            this.Controls.Add(this.memberID_label);
            this.Controls.Add(this.clearSelection_button);
            this.Controls.Add(this.personSelection_comboBox);
            this.Controls.Add(this.selectPerson_label);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "SinglePayrollForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin SinglePayroll";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SinglePayrollForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label selectPerson_label;
        private System.Windows.Forms.ComboBox personSelection_comboBox;
        private System.Windows.Forms.Button clearSelection_button;
        private System.Windows.Forms.Label memberID_label;
        private System.Windows.Forms.Label fullName_label;
        private System.Windows.Forms.Label dateOfEmployment_label;
        private System.Windows.Forms.Label position_label;
        private System.Windows.Forms.Label baseRate_label;
        private System.Windows.Forms.Label yearsWithCompany_label;
        private System.Windows.Forms.Label supervises_label;
        private System.Windows.Forms.TextBox memberID_textBox;
        private System.Windows.Forms.TextBox fullName_textBox;
        private System.Windows.Forms.TextBox dateOfEmployment_textBox;
        private System.Windows.Forms.TextBox position_textBox;
        private System.Windows.Forms.TextBox baseRate_textBox;
        private System.Windows.Forms.TextBox yearsWithCompany_textBox;
        private System.Windows.Forms.TextBox numberOfInferiors_textBox;
        private System.Windows.Forms.Label payrollStart_label;
        private System.Windows.Forms.Label payrollEnd_label;
        private System.Windows.Forms.DateTimePicker startDate_dateTimePicker;
        private System.Windows.Forms.DateTimePicker endDate_dateTimePicker;
        private System.Windows.Forms.Button calculatePayroll_button;
        private System.Windows.Forms.Label totalWorkdays_label;
        private System.Windows.Forms.Label totalEarnings_label;
        private System.Windows.Forms.TextBox totalWorkdays_textBox;
        private System.Windows.Forms.TextBox totalEarnings_textBox;
        private System.Windows.Forms.Button displayInferiors_button;
        private System.Windows.Forms.Label basePay_label;
        private System.Windows.Forms.TextBox basePay_textBox;
        private System.Windows.Forms.TextBox yearBonus_textBox;
        private System.Windows.Forms.Label yearBonus_label;
        private System.Windows.Forms.TextBox referralBonusPay_textBox;
        private System.Windows.Forms.Label referralBonusPay_label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchToMultiPayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}

