﻿using System;
using System.Windows.Forms;

namespace GUI
{
    /// <summary>
    /// The application's main class
    /// </summary>
    static class HRMPayrollApp
    {
        /// <summary>
        /// The entry point for the application
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // LoginForm is the application's entry form
            Application.Run(LoginForm.GetLoginForm());
        }
    }
}
