﻿namespace GUI
{
    partial class AdminPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminPanelForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singlePayroll_button = new System.Windows.Forms.Button();
            this.multiPayroll_button = new System.Windows.Forms.Button();
            this.addUser_button = new System.Windows.Forms.Button();
            this.removeUser_button = new System.Windows.Forms.Button();
            this.checkLog_button = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Enabled = false;
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // singlePayroll_button
            // 
            this.singlePayroll_button.Location = new System.Drawing.Point(286, 45);
            this.singlePayroll_button.Name = "singlePayroll_button";
            this.singlePayroll_button.Size = new System.Drawing.Size(236, 55);
            this.singlePayroll_button.TabIndex = 23;
            this.singlePayroll_button.Text = "Single Payroll";
            this.singlePayroll_button.UseVisualStyleBackColor = true;
            this.singlePayroll_button.Click += new System.EventHandler(this.singlePayroll_button_Click);
            // 
            // multiPayroll_button
            // 
            this.multiPayroll_button.Location = new System.Drawing.Point(286, 127);
            this.multiPayroll_button.Name = "multiPayroll_button";
            this.multiPayroll_button.Size = new System.Drawing.Size(236, 55);
            this.multiPayroll_button.TabIndex = 24;
            this.multiPayroll_button.Text = "Multi Payroll";
            this.multiPayroll_button.UseVisualStyleBackColor = true;
            this.multiPayroll_button.Click += new System.EventHandler(this.multiPayroll_button_Click);
            // 
            // addUser_button
            // 
            this.addUser_button.Location = new System.Drawing.Point(286, 212);
            this.addUser_button.Name = "addUser_button";
            this.addUser_button.Size = new System.Drawing.Size(236, 55);
            this.addUser_button.TabIndex = 25;
            this.addUser_button.Text = "Add Worker";
            this.addUser_button.UseVisualStyleBackColor = true;
            this.addUser_button.Click += new System.EventHandler(this.addUser_button_Click);
            // 
            // removeUser_button
            // 
            this.removeUser_button.Location = new System.Drawing.Point(286, 296);
            this.removeUser_button.Name = "removeUser_button";
            this.removeUser_button.Size = new System.Drawing.Size(236, 55);
            this.removeUser_button.TabIndex = 26;
            this.removeUser_button.Text = "Remove Worker";
            this.removeUser_button.UseVisualStyleBackColor = true;
            this.removeUser_button.Click += new System.EventHandler(this.removeUser_button_Click);
            // 
            // checkLog_button
            // 
            this.checkLog_button.Location = new System.Drawing.Point(286, 373);
            this.checkLog_button.Name = "checkLog_button";
            this.checkLog_button.Size = new System.Drawing.Size(236, 55);
            this.checkLog_button.TabIndex = 27;
            this.checkLog_button.Text = "Check Log";
            this.checkLog_button.UseVisualStyleBackColor = true;
            this.checkLog_button.Click += new System.EventHandler(this.checkLog_button_Click);
            // 
            // AdminPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkLog_button);
            this.Controls.Add(this.removeUser_button);
            this.Controls.Add(this.addUser_button);
            this.Controls.Add(this.multiPayroll_button);
            this.Controls.Add(this.singlePayroll_button);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AdminPanelForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin Panel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminPanelForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.Button singlePayroll_button;
        private System.Windows.Forms.Button multiPayroll_button;
        private System.Windows.Forms.Button addUser_button;
        private System.Windows.Forms.Button removeUser_button;
        private System.Windows.Forms.Button checkLog_button;
    }
}