﻿using System;
using System.Windows.Forms;

namespace GUI
{
    /// <summary>
    /// Form where Admin can choose what to do next and summon the appropriate form
    /// </summary>
    public partial class AdminPanelForm : Form
    {
        /// <summary>adminPanelForm is the only allowed instance of AdminPanelForm</summary>
        private static AdminPanelForm adminPanelForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminPanelForm instance
        /// </summary>
        /// <returns>The existing instance of AdminPanelForm or a new AdminPanelForm if such doesn't exist</returns>
        public static AdminPanelForm GetAdminPanelForm()
        {
            if (adminPanelForm == null)
            {
                return new AdminPanelForm();
            }
            else
            {
                return adminPanelForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminSinglePayrollForm elements
        /// </summary>
        private AdminPanelForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void singlePayroll_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminSinglePayrollForm.GetAdminSinglePayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void multiPayroll_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void addUser_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminAddWorkerForm.GetAdminAddWorkerForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void removeUser_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminRemoveWorkerForm.GetAdminRemoveWorkerForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void AdminPanelForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void checkLog_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminLogForm.GetAdminLogForm();
            that.Show();
            this.Dispose();
        }
    }
}
