﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Base;
using Data;
using Service;

namespace GUI
{
    /// <summary>
    /// Form where Admin can remove Persons/Workers from the DB
    /// </summary>
    public partial class AdminRemoveWorkerForm : Form
    {
        /// <summary>_RemovableUserID is the ID of User who is to be removed</summary>
        private int _RemovableUserID { get; set; }
        /// <summary>_RemovablePerson is the User object whose information is to be removed</summary>
        private Person _RemovablePerson { get; set; }
        /// <summary>ListOfInferiorsTooltip is a list of _RemovablePerson's inferiors</summary>
        private ToolTip ListOfInferiorsTooltip { get; set; }
        /// <summary>adminRemoveWorkerForm is the only allowed instance of AdminRemoveWorkerForm</summary>
        private static AdminRemoveWorkerForm adminRemoveWorkerForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminRemoveWorkerForm instance
        /// </summary>
        /// <returns>The existing instance of AdminRemoveWorkerForm or a new AdminRemoveWorkerForm if such doesn't exist</returns>
        public static AdminRemoveWorkerForm GetAdminRemoveWorkerForm()
        {
            if (adminRemoveWorkerForm == null)
            {
                return new AdminRemoveWorkerForm();
            }
            else
            {
                return adminRemoveWorkerForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminRemoveWorkerForm elements
        /// </summary>
        private AdminRemoveWorkerForm()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        /// <summary>
        /// Custom initializer, refreshes the form's UI elements
        /// </summary>
        private void InitializeComponent2()
        {
            allWorkers_comboBox.Items.Clear();
            allWorkers_comboBox.Text = "Select...";
            memberID_textBox.Text = "";
            fullName_textBox.Text = "";
            dateOfEmployment_textBox.Text = "";
            position_textBox.Text = "";
            baseRate_textBox.Text = "";
            yearsWithCompany_textBox.Text = "";
            numberOfInferiors_textBox.Text = "";
            LoadPersons();
            ListOfInferiorsTooltip = new ToolTip();
            removeWorker_button.Visible = false;
            supervises_label.Visible = false;
            numberOfInferiors_textBox.Visible = false;
            displayInferiors_button.Visible = false;
        }

        /// <summary>
        /// Fills up the drop-down list with information about each member of the company staff
        /// </summary>
        private void LoadPersons()
        {
            try
            {
                foreach (var person in PersonFetcher.FetchAll())
                {
                    allWorkers_comboBox.Items.Add($"{person.MemberID}: {person.FullName} - {person.Position}");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Finds the _RemovablePerson object by ID and displays their basic info
        /// </summary>
        private void DisplayRemovablePersonInfo()
        {
            try
            {
                // _RemovablePerson is identified
                _RemovablePerson = PersonFetcher.FetchAll().Where(p => p.MemberID == _RemovableUserID).Select(p => p).First();
                // Fills the text fields with information about the Person object
                memberID_textBox.Text = $"{_RemovablePerson.MemberID.ToString()}";
                fullName_textBox.Text = $"{_RemovablePerson.FullName}";
                dateOfEmployment_textBox.Text = $"{_RemovablePerson.DateOfEmployment.ToShortDateString()}";
                position_textBox.Text = $"{_RemovablePerson.Position}";
                baseRate_textBox.Text = $"{_RemovablePerson.BaseHourlyRate.ToString()}";
                yearsWithCompany_textBox.Text = _RemovablePerson.GetYearsOfEmployment(DateTime.Now).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to load elements");
            }

            // Managers and Salesmen can have inferiors, while Employees can't, this part reflects that
            if (_RemovablePerson != null && _RemovablePerson.Position == "Manager")
            {
                supervises_label.Visible = true;
                numberOfInferiors_textBox.Visible = true;
                displayInferiors_button.Visible = true;
                LoadInferiorsIntoTooltip();
            }
            else if (_RemovablePerson != null && _RemovablePerson.Position == "Salesman")
            {
                supervises_label.Visible = true;
                numberOfInferiors_textBox.Visible = true;
                displayInferiors_button.Visible = true;
                LoadInferiorsIntoTooltip();
            }
            else
            {
                supervises_label.Visible = false;
                numberOfInferiors_textBox.Visible = false;
                displayInferiors_button.Visible = false;
            }
        }

        /// <summary>
        /// Loads information about the selected superior's inferiors into the tooltip
        /// </summary>
        private void LoadInferiorsIntoTooltip()
        {
            var sb = new StringBuilder();
            var countOfInferiors = default(int);

            try
            {
                foreach (var person in PersonFetcher.FetchInferiors(_RemovablePerson.MemberID))
                {
                    sb.Append($"{person.MemberID}: {person.FullName} - {person.Position}\n");
                    countOfInferiors++;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
            ListOfInferiorsTooltip.SetToolTip(displayInferiors_button, sb.ToString());
            numberOfInferiors_textBox.Text = countOfInferiors.ToString();
        }

        /// <summary>
        /// Passes _RemovableWorker's ID to the appropriate method and notifies User if the attempt was successful
        /// </summary>
        private void AttemptToRemoveWorker()
        {
            var result = PersonFetcher.RemoveWorker(_RemovableUserID);
            // If the response is positive, the list of all workers and the corresponding drop-down comboBox are updated and the fields are reset
            if (result == true)
            {
                ActionLogger.LogAction($"successfully removed [worker {_RemovableUserID}]", EntityIdHolder.AdminID);
                MessageBox.Show("User successfully removed!");
                InitializeComponent2();
            }
            // If the response is negative, Form notifies Admin
            else
            {
                ActionLogger.LogAction($"failed to remove [worker {_RemovableUserID}]", EntityIdHolder.AdminID);
                MessageBox.Show("User was not removed!");
            }
            // All UI elements are refreshed regardless of the result
            InitializeComponent2();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void goToMainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminPanelForm.GetAdminPanelForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void multiPayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void singlePayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminSinglePayrollForm.GetAdminSinglePayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.AdminID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void AdminRemoveWorkerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Enables the button when Worker is selected for removal, assigns that worker's ID to the form's _RemovableUserID property
        /// </summary>
        private void allWorkers_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeWorker_button.Visible = true;
            _RemovableUserID = int.Parse(allWorkers_comboBox.SelectedItem.ToString().Split(':')[0]);
            DisplayRemovablePersonInfo();
            LoadInferiorsIntoTooltip();
        }

        /// <summary>
        /// Initiates the removal process by calling the appropriate method
        /// </summary>
        private void removeWorker_button_Click(object sender, EventArgs e)
        {
            AttemptToRemoveWorker();
        }
    }
}
