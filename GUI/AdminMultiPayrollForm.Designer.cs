﻿namespace GUI
{
    partial class AdminMultiPayrollForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMultiPayrollForm));
            this.allStaff_gridView = new System.Windows.Forms.DataGridView();
            this.personID_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullName_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateOfEmployment_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.position_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.baseRate_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.singlePayroll_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calculatePayroll_button = new System.Windows.Forms.Button();
            this.endDate_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.payrollEnd_label = new System.Windows.Forms.Label();
            this.payrollStart_label = new System.Windows.Forms.Label();
            this.getAllStaff_button = new System.Windows.Forms.Button();
            this.referralBonuses_textBox = new System.Windows.Forms.TextBox();
            this.referralBonusPay_label = new System.Windows.Forms.Label();
            this.yearBonuses_textBox = new System.Windows.Forms.TextBox();
            this.yearBonus_label = new System.Windows.Forms.Label();
            this.totalEarnings_textBox = new System.Windows.Forms.TextBox();
            this.totalWorkdays_textBox = new System.Windows.Forms.TextBox();
            this.totalEarnings_label = new System.Windows.Forms.Label();
            this.totalWorkdays_label = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu_menuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchToSinglePayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allStaff_label = new System.Windows.Forms.Label();
            this.totalBasePay_textBox = new System.Windows.Forms.TextBox();
            this.totalBasePay_label = new System.Windows.Forms.Label();
            this.clearSelection_button = new System.Windows.Forms.Button();
            this.startDate_dateTimePicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.allStaff_gridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // allStaff_gridView
            // 
            this.allStaff_gridView.AllowUserToAddRows = false;
            this.allStaff_gridView.AllowUserToDeleteRows = false;
            this.allStaff_gridView.AllowUserToResizeColumns = false;
            this.allStaff_gridView.AllowUserToResizeRows = false;
            this.allStaff_gridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.allStaff_gridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.allStaff_gridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allStaff_gridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.personID_column,
            this.fullName_column,
            this.dateOfEmployment_column,
            this.position_column,
            this.baseRate_column,
            this.singlePayroll_column});
            this.allStaff_gridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.allStaff_gridView.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.allStaff_gridView.Location = new System.Drawing.Point(21, 66);
            this.allStaff_gridView.MultiSelect = false;
            this.allStaff_gridView.Name = "allStaff_gridView";
            this.allStaff_gridView.ReadOnly = true;
            this.allStaff_gridView.RowHeadersVisible = false;
            this.allStaff_gridView.Size = new System.Drawing.Size(489, 316);
            this.allStaff_gridView.TabIndex = 0;
            // 
            // personID_column
            // 
            this.personID_column.FillWeight = 49.77076F;
            this.personID_column.HeaderText = "ID";
            this.personID_column.Name = "personID_column";
            this.personID_column.ReadOnly = true;
            this.personID_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // fullName_column
            // 
            this.fullName_column.FillWeight = 121.8274F;
            this.fullName_column.HeaderText = "Full Name";
            this.fullName_column.Name = "fullName_column";
            this.fullName_column.ReadOnly = true;
            this.fullName_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dateOfEmployment_column
            // 
            this.dateOfEmployment_column.FillWeight = 110.1178F;
            this.dateOfEmployment_column.HeaderText = "Date Of Employment";
            this.dateOfEmployment_column.Name = "dateOfEmployment_column";
            this.dateOfEmployment_column.ReadOnly = true;
            this.dateOfEmployment_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // position_column
            // 
            this.position_column.FillWeight = 110.1178F;
            this.position_column.HeaderText = "Position";
            this.position_column.Name = "position_column";
            this.position_column.ReadOnly = true;
            this.position_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // baseRate_column
            // 
            this.baseRate_column.FillWeight = 110.1178F;
            this.baseRate_column.HeaderText = "Base Hourly Rate";
            this.baseRate_column.Name = "baseRate_column";
            this.baseRate_column.ReadOnly = true;
            this.baseRate_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // singlePayroll_column
            // 
            this.singlePayroll_column.FillWeight = 98.04838F;
            this.singlePayroll_column.HeaderText = "Full Payroll";
            this.singlePayroll_column.Name = "singlePayroll_column";
            this.singlePayroll_column.ReadOnly = true;
            this.singlePayroll_column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // calculatePayroll_button
            // 
            this.calculatePayroll_button.Enabled = false;
            this.calculatePayroll_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculatePayroll_button.Location = new System.Drawing.Point(539, 137);
            this.calculatePayroll_button.Name = "calculatePayroll_button";
            this.calculatePayroll_button.Size = new System.Drawing.Size(200, 28);
            this.calculatePayroll_button.TabIndex = 26;
            this.calculatePayroll_button.Text = "Calculate Payroll";
            this.calculatePayroll_button.UseVisualStyleBackColor = true;
            this.calculatePayroll_button.Click += new System.EventHandler(this.calculatePayroll_button_Click);
            // 
            // endDate_dateTimePicker
            // 
            this.endDate_dateTimePicker.Location = new System.Drawing.Point(539, 111);
            this.endDate_dateTimePicker.Name = "endDate_dateTimePicker";
            this.endDate_dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDate_dateTimePicker.TabIndex = 25;
            this.endDate_dateTimePicker.ValueChanged += new System.EventHandler(this.endDate_dateTimePicker_ValueChanged_1);
            // 
            // payrollEnd_label
            // 
            this.payrollEnd_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.payrollEnd_label.AutoSize = true;
            this.payrollEnd_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollEnd_label.Location = new System.Drawing.Point(535, 89);
            this.payrollEnd_label.Name = "payrollEnd_label";
            this.payrollEnd_label.Size = new System.Drawing.Size(218, 19);
            this.payrollEnd_label.TabIndex = 23;
            this.payrollEnd_label.Text = "Specify Payroll End Date:";
            // 
            // payrollStart_label
            // 
            this.payrollStart_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.payrollStart_label.AutoSize = true;
            this.payrollStart_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollStart_label.Location = new System.Drawing.Point(526, 43);
            this.payrollStart_label.Name = "payrollStart_label";
            this.payrollStart_label.Size = new System.Drawing.Size(226, 19);
            this.payrollStart_label.TabIndex = 22;
            this.payrollStart_label.Text = "Specify Payroll Start Date:";
            // 
            // getAllStaff_button
            // 
            this.getAllStaff_button.Location = new System.Drawing.Point(156, 395);
            this.getAllStaff_button.Name = "getAllStaff_button";
            this.getAllStaff_button.Size = new System.Drawing.Size(215, 30);
            this.getAllStaff_button.TabIndex = 27;
            this.getAllStaff_button.Text = "Get All Staff";
            this.getAllStaff_button.UseVisualStyleBackColor = true;
            this.getAllStaff_button.Click += new System.EventHandler(this.getAllStaff_button_Click);
            // 
            // referralBonuses_textBox
            // 
            this.referralBonuses_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referralBonuses_textBox.Location = new System.Drawing.Point(533, 344);
            this.referralBonuses_textBox.Name = "referralBonuses_textBox";
            this.referralBonuses_textBox.ReadOnly = true;
            this.referralBonuses_textBox.Size = new System.Drawing.Size(214, 23);
            this.referralBonuses_textBox.TabIndex = 45;
            this.referralBonuses_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.referralBonuses_textBox.Visible = false;
            // 
            // referralBonusPay_label
            // 
            this.referralBonusPay_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.referralBonusPay_label.AutoSize = true;
            this.referralBonusPay_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referralBonusPay_label.Location = new System.Drawing.Point(571, 322);
            this.referralBonusPay_label.Name = "referralBonusPay_label";
            this.referralBonusPay_label.Size = new System.Drawing.Size(153, 19);
            this.referralBonusPay_label.TabIndex = 44;
            this.referralBonusPay_label.Text = "Referral Bonuses:";
            this.referralBonusPay_label.Visible = false;
            // 
            // yearBonuses_textBox
            // 
            this.yearBonuses_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearBonuses_textBox.Location = new System.Drawing.Point(533, 296);
            this.yearBonuses_textBox.Name = "yearBonuses_textBox";
            this.yearBonuses_textBox.ReadOnly = true;
            this.yearBonuses_textBox.Size = new System.Drawing.Size(214, 23);
            this.yearBonuses_textBox.TabIndex = 43;
            this.yearBonuses_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yearBonuses_textBox.Visible = false;
            // 
            // yearBonus_label
            // 
            this.yearBonus_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yearBonus_label.AutoSize = true;
            this.yearBonus_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearBonus_label.Location = new System.Drawing.Point(558, 274);
            this.yearBonus_label.Name = "yearBonus_label";
            this.yearBonus_label.Size = new System.Drawing.Size(176, 19);
            this.yearBonus_label.TabIndex = 42;
            this.yearBonus_label.Text = "Year-based Bonuses:";
            this.yearBonus_label.Visible = false;
            // 
            // totalEarnings_textBox
            // 
            this.totalEarnings_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalEarnings_textBox.Location = new System.Drawing.Point(534, 392);
            this.totalEarnings_textBox.Name = "totalEarnings_textBox";
            this.totalEarnings_textBox.ReadOnly = true;
            this.totalEarnings_textBox.Size = new System.Drawing.Size(214, 23);
            this.totalEarnings_textBox.TabIndex = 39;
            this.totalEarnings_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalEarnings_textBox.Visible = false;
            // 
            // totalWorkdays_textBox
            // 
            this.totalWorkdays_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalWorkdays_textBox.Location = new System.Drawing.Point(534, 201);
            this.totalWorkdays_textBox.Name = "totalWorkdays_textBox";
            this.totalWorkdays_textBox.ReadOnly = true;
            this.totalWorkdays_textBox.Size = new System.Drawing.Size(214, 23);
            this.totalWorkdays_textBox.TabIndex = 38;
            this.totalWorkdays_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalWorkdays_textBox.Visible = false;
            // 
            // totalEarnings_label
            // 
            this.totalEarnings_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalEarnings_label.AutoSize = true;
            this.totalEarnings_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalEarnings_label.Location = new System.Drawing.Point(530, 370);
            this.totalEarnings_label.Name = "totalEarnings_label";
            this.totalEarnings_label.Size = new System.Drawing.Size(226, 19);
            this.totalEarnings_label.TabIndex = 37;
            this.totalEarnings_label.Text = "Total Earnings For Period:";
            this.totalEarnings_label.Visible = false;
            // 
            // totalWorkdays_label
            // 
            this.totalWorkdays_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalWorkdays_label.AutoSize = true;
            this.totalWorkdays_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalWorkdays_label.Location = new System.Drawing.Point(530, 179);
            this.totalWorkdays_label.Name = "totalWorkdays_label";
            this.totalWorkdays_label.Size = new System.Drawing.Size(224, 19);
            this.totalWorkdays_label.TabIndex = 36;
            this.totalWorkdays_label.Text = "Total Workdays In Period:";
            this.totalWorkdays_label.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_menuStrip,
            this.switchToSinglePayrollToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 47;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu_menuStrip
            // 
            this.menu_menuStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menu_menuStrip.Name = "menu_menuStrip";
            this.menu_menuStrip.Size = new System.Drawing.Size(50, 20);
            this.menu_menuStrip.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            this.goToMainMenuToolStripMenuItem.Click += new System.EventHandler(this.goToMainMenuToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // switchToSinglePayrollToolStripMenuItem
            // 
            this.switchToSinglePayrollToolStripMenuItem.Name = "switchToSinglePayrollToolStripMenuItem";
            this.switchToSinglePayrollToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.switchToSinglePayrollToolStripMenuItem.Text = "Switch To SinglePayroll";
            this.switchToSinglePayrollToolStripMenuItem.Click += new System.EventHandler(this.switchToSinglePayrollToolStripMenuItem_Click);
            // 
            // allStaff_label
            // 
            this.allStaff_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allStaff_label.AutoSize = true;
            this.allStaff_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allStaff_label.Location = new System.Drawing.Point(211, 39);
            this.allStaff_label.Name = "allStaff_label";
            this.allStaff_label.Size = new System.Drawing.Size(80, 19);
            this.allStaff_label.TabIndex = 48;
            this.allStaff_label.Text = "All Staff:";
            // 
            // totalBasePay_textBox
            // 
            this.totalBasePay_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalBasePay_textBox.Location = new System.Drawing.Point(534, 247);
            this.totalBasePay_textBox.Name = "totalBasePay_textBox";
            this.totalBasePay_textBox.ReadOnly = true;
            this.totalBasePay_textBox.Size = new System.Drawing.Size(214, 23);
            this.totalBasePay_textBox.TabIndex = 50;
            this.totalBasePay_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalBasePay_textBox.Visible = false;
            // 
            // totalBasePay_label
            // 
            this.totalBasePay_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totalBasePay_label.AutoSize = true;
            this.totalBasePay_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalBasePay_label.Location = new System.Drawing.Point(574, 225);
            this.totalBasePay_label.Name = "totalBasePay_label";
            this.totalBasePay_label.Size = new System.Drawing.Size(135, 19);
            this.totalBasePay_label.TabIndex = 49;
            this.totalBasePay_label.Text = "Total Base Pay:";
            this.totalBasePay_label.Visible = false;
            // 
            // clearSelection_button
            // 
            this.clearSelection_button.BackColor = System.Drawing.Color.Lime;
            this.clearSelection_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearSelection_button.Location = new System.Drawing.Point(297, 38);
            this.clearSelection_button.Name = "clearSelection_button";
            this.clearSelection_button.Size = new System.Drawing.Size(55, 22);
            this.clearSelection_button.TabIndex = 51;
            this.clearSelection_button.Text = "Clear";
            this.clearSelection_button.UseVisualStyleBackColor = false;
            this.clearSelection_button.Click += new System.EventHandler(this.clearSelection_button_Click);
            // 
            // startDate_dateTimePicker
            // 
            this.startDate_dateTimePicker.Location = new System.Drawing.Point(539, 66);
            this.startDate_dateTimePicker.Name = "startDate_dateTimePicker";
            this.startDate_dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDate_dateTimePicker.TabIndex = 52;
            this.startDate_dateTimePicker.ValueChanged += new System.EventHandler(this.startDate_dateTimePicker_ValueChanged);
            // 
            // AdminMultiPayrollForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.startDate_dateTimePicker);
            this.Controls.Add(this.clearSelection_button);
            this.Controls.Add(this.totalBasePay_textBox);
            this.Controls.Add(this.totalBasePay_label);
            this.Controls.Add(this.allStaff_label);
            this.Controls.Add(this.referralBonuses_textBox);
            this.Controls.Add(this.referralBonusPay_label);
            this.Controls.Add(this.yearBonuses_textBox);
            this.Controls.Add(this.yearBonus_label);
            this.Controls.Add(this.totalEarnings_textBox);
            this.Controls.Add(this.totalWorkdays_textBox);
            this.Controls.Add(this.totalEarnings_label);
            this.Controls.Add(this.totalWorkdays_label);
            this.Controls.Add(this.getAllStaff_button);
            this.Controls.Add(this.calculatePayroll_button);
            this.Controls.Add(this.endDate_dateTimePicker);
            this.Controls.Add(this.payrollEnd_label);
            this.Controls.Add(this.payrollStart_label);
            this.Controls.Add(this.allStaff_gridView);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "AdminMultiPayrollForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin MultiPayroll";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MultiPayrollForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.allStaff_gridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView allStaff_gridView;
        private System.Windows.Forms.Button calculatePayroll_button;
        private System.Windows.Forms.DateTimePicker endDate_dateTimePicker;
        private System.Windows.Forms.Label payrollEnd_label;
        private System.Windows.Forms.Label payrollStart_label;
        private System.Windows.Forms.Button getAllStaff_button;
        private System.Windows.Forms.TextBox referralBonuses_textBox;
        private System.Windows.Forms.Label referralBonusPay_label;
        private System.Windows.Forms.TextBox yearBonuses_textBox;
        private System.Windows.Forms.Label yearBonus_label;
        private System.Windows.Forms.TextBox totalEarnings_textBox;
        private System.Windows.Forms.TextBox totalWorkdays_textBox;
        private System.Windows.Forms.Label totalEarnings_label;
        private System.Windows.Forms.Label totalWorkdays_label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_menuStrip;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchToSinglePayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.Label allStaff_label;
        private System.Windows.Forms.TextBox totalBasePay_textBox;
        private System.Windows.Forms.Label totalBasePay_label;
        private System.Windows.Forms.Button clearSelection_button;
        private System.Windows.Forms.DataGridViewTextBoxColumn personID_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullName_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateOfEmployment_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn position_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn baseRate_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn singlePayroll_column;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker startDate_dateTimePicker;
    }
}