﻿using System;
using System.Windows.Forms;
using Data;
using Service;

namespace GUI
{
    /// <summary>
    /// Form where Admin can see and dispose information about actions all Users performed
    /// </summary>
    public partial class AdminLogForm : Form
    {
        /// <summary>adminLogForm is the only allowed instance of AdminLogForm</summary>
        private static AdminLogForm adminLogForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminLogForm instance
        /// </summary>
        /// <returns>The existing instance of AdminLogForm or a new AdminLogForm if such doesn't exist</returns>
        public static AdminLogForm GetAdminLogForm()
        {
            if (adminLogForm == null)
            {
                return new AdminLogForm();
            }
            else
            {
                return adminLogForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminSinglePayrollForm elements
        /// </summary>
        private AdminLogForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Renders log entries on the screen
        /// </summary>
        private void GetLog()
        {
            log_dataGridView.Rows.Clear();
            var entries = ActionLogger.RetrieveLogRecords();

            try
            {
                for (var i = 0; i < entries.Count; i++)
                {
                    log_dataGridView.Rows.Add();
                    if (entries[i].UserId == -1)
                    {
                        log_dataGridView.Rows[i].Cells[0].Value = "N/A";
                    }
                    else
                    {
                        log_dataGridView.Rows[i].Cells[0].Value = entries[i].UserId.ToString();
                    }
                    log_dataGridView.Rows[i].Cells[1].Value = entries[i].Timestamp.ToLocalTime().ToString();
                    log_dataGridView.Rows[i].Cells[2].Value = entries[i].Action;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Calls the ClearLog method and notifies User of the result; re-renders log entries in any case
        /// </summary>
        public void ClearLog()
        {
            if (ActionLogger.ClearLog() == true)
            {
                GetLog();
                MessageBox.Show("Log cleared");
            }
            else
            {
                ActionLogger.LogAction("failed to clear log", EntityIdHolder.AdminID);
                GetLog();
                MessageBox.Show("Unable to clear log");
            }
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void goToMainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminPanelForm.GetAdminPanelForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.AdminID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void singlePayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminSinglePayrollForm.GetAdminSinglePayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void multiPayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Calls the GetLog method
        /// </summary>
        private void getLog_button_Click(object sender, EventArgs e)
        {
            GetLog();
        }

        /// <summary>
        /// Makes sure that when the user closes the window, the application really stops working
        /// </summary>
        private void AdminLogForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Calls the ClearLog method
        /// </summary>
        private void clearLog_button_Click(object sender, EventArgs e)
        {
            ClearLog();
        }
    }
}
