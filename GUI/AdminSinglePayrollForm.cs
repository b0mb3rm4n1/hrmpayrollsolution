﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Base;
using Data;
using Core;
using Service;

namespace GUI
{
    /// <summary>
    /// Form where Admin can view time-specific payroll information about each individual _Worker 
    /// </summary>
    public partial class AdminSinglePayrollForm : Form
    {
        /// <summary>_Persons is a list of all company staff</summary>
        private static List<Person> _Persons { get; set; }
        /// <summary>_SelectedPerson is the currently selected Person object</summary>
        private Person _SelectedPerson { get; set; }
        /// <summary>RefreshSelectionTooltip is a tooltip saying that the button to which the tooltip is attached refreshes the UI</summary>
        private ToolTip RefreshSelectionTooltip { get; }
        /// <summary>ListOfInferiorsTooltip is a tooltip saying that the button to which the tooltip is attached refreshes the UI</summary>
        private ToolTip ListOfInferiorsTooltip { get; }
        /// <summary>adminSinglePayrollForm is the only allowed instance of AdminSinglePayrollForm</summary>
        private static AdminSinglePayrollForm adminSinglePayrollForm { get; set; }

        /// <summary>
        /// Fetches or creates AdminSinglePayrollForm instance
        /// </summary>
        /// <returns>The existing instance of AdminSinglePayrollForm or a new AdminSinglePayrollForm if such doesn't exist</returns>
        public static AdminSinglePayrollForm GetAdminSinglePayrollForm()
        {
            if (adminSinglePayrollForm == null)
            {
                return new AdminSinglePayrollForm();
            }
            else
            {
                return adminSinglePayrollForm;
            }
        }

        /// <summary>
        /// Constructor, sets off the initialization of AdminSinglePayrollForm elements and fills the _Persons list with objects
        /// </summary>
        private AdminSinglePayrollForm()
        {
            InitializeComponent();
            InitializeComponent2();
            _Persons = PersonFetcher.FetchAll();
            LoadPersons();
            RefreshSelectionTooltip = new ToolTip();
            RefreshSelectionTooltip.SetToolTip(clearSelection_button, "Clear All");
            // Fills later upon Person selection
            ListOfInferiorsTooltip = new ToolTip();
        }

        /// <summary>
        /// Custom initializer, refreshes the form's UI elements
        /// </summary>
        private void InitializeComponent2()
        {
            // Calendar is unavailable until Admin selects Person
            startDate_dateTimePicker.Enabled = false;
            endDate_dateTimePicker.Enabled = false;
            // Payroll information-related fields are invisible until Admin selects Person, payroll dates, and presses the button
            totalEarnings_label.Visible = false;
            totalEarnings_textBox.Visible = false;
            basePay_label.Visible = false;
            basePay_textBox.Visible = false;
            yearBonus_label.Visible = false;
            yearBonus_textBox.Visible = false;
            referralBonusPay_label.Visible = false;
            referralBonusPay_textBox.Visible = false;
            // By default, the DateTimePicker controls' values are set to today(now, to be exact)
            endDate_dateTimePicker.Value = DateTime.Now;
            startDate_dateTimePicker.Value = DateTime.Now;
            startDate_dateTimePicker.MaxDate = DateTime.Now;
            endDate_dateTimePicker.MaxDate = DateTime.Now;
            yearsWithCompany_textBox.Text = "";
            UpdateWorkdays();
        }

        /// <summary>
        /// Finds Person in the _Persons list by ID and displays their basic info
        /// </summary>
        private void DisplaySelectedPersonInfo()
        {
            try
            {
                // The selected Person's ID is parsed
                var selectedID = personSelection_comboBox.SelectedItem.ToString().Split(':')[0];
                // The Form's Person property is linked to the Person object with the specified ID contained in the _Persons list  
                _SelectedPerson = _Persons.Where(p => p.MemberID.ToString() == selectedID).Select(p => p).First();
                // And cast into the appropriate derived type depending on its Position property
                _SelectedPerson = ClassConverter.ConvertBaseToDerived(_SelectedPerson);
                // The DateTimePicker controls' Min and Max Dates are immediately adjusted to the selected Person to reflect their actual employment time windows
                startDate_dateTimePicker.MinDate = DateTime.Parse(_SelectedPerson.DateOfEmployment.ToString());
                startDate_dateTimePicker.MaxDate = DateTime.Now;
                endDate_dateTimePicker.MinDate = DateTime.Parse(_SelectedPerson.DateOfEmployment.ToString());
                endDate_dateTimePicker.MaxDate = DateTime.Now;
                // After Person has been successfully determined, the DateTimePicker objects, the Calculate button become available for use
                startDate_dateTimePicker.Enabled = true;
                endDate_dateTimePicker.Enabled = true;
                calculatePayroll_button.Enabled = true;
                // And the number of workdays between the dates becomes visible (the number is already known by the time this method is called)
                totalWorkdays_label.Visible = true;
                totalWorkdays_textBox.Visible = true;
                // The text fields are filled accordingly
                memberID_textBox.Text = $"{_SelectedPerson.MemberID.ToString()}";
                fullName_textBox.Text = $"{_SelectedPerson.FullName}";
                dateOfEmployment_textBox.Text = $"{_SelectedPerson.DateOfEmployment.ToShortDateString()}";
                position_textBox.Text = $"{_SelectedPerson.Position}";
                baseRate_textBox.Text = $"{_SelectedPerson.BaseHourlyRate.ToString()}";
                AdjustYearsWithCompany();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to load elements");
            }

            // Managers and Salesmen can have inferiors, while Employees can't, unnecessary fields aren't shown
            if (_SelectedPerson != null && _SelectedPerson.Position == "Manager")
            {
                supervises_label.Visible = true;
                numberOfInferiors_textBox.Visible = true;
                displayInferiors_button.Visible = true;
                numberOfInferiors_textBox.Text = $"{(_SelectedPerson as Manager)?.ListOfInferiorEmployees.Count}";
                LoadInferiorsIntoTooltip();
            }
            else if (_SelectedPerson != null && _SelectedPerson.Position == "Salesman")
            {
                supervises_label.Visible = true;
                numberOfInferiors_textBox.Visible = true;
                displayInferiors_button.Visible = true;
                numberOfInferiors_textBox.Text = $"{(_SelectedPerson as Salesman)?.ListOfInferiorPersons.Count}";
                LoadInferiorsIntoTooltip();
            }
            else
            {
                supervises_label.Visible = false;
                numberOfInferiors_textBox.Visible = false;
                displayInferiors_button.Visible = false;
            }
        }

        /// <summary>
        /// Fills up the drop-down list with information about each Person
        /// </summary>
        private void LoadPersons()
        {
            try
            {
                foreach (var person in _Persons)
                {
                    personSelection_comboBox.Items.Add($"{person.MemberID}: {person.FullName}");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Refreshes certain UI elements depending on context
        /// </summary>
        private void UpdateWorkdays()
        {
            // If an illegal time span is selected, the DateTimePicker objects' values are reset
            if (startDate_dateTimePicker.Value > endDate_dateTimePicker.Value)
            {
                ResetCalendars();
            }
            // If the time span is OK, the number of workdays between the two dates is calculated and displayed
            else
            {
                totalWorkdays_textBox.Text = WorkdayCounter.CountWorkdays(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value).ToString();
            }
        }

        /// <summary>
        /// Refreshes the UI to reflect how many years Worker had worked for the company by the date selected in the enddate DateTimePicker control
        /// </summary>
        private void AdjustYearsWithCompany()
        {
            if (_SelectedPerson != null)
            {
                yearsWithCompany_textBox.Text = _SelectedPerson.GetYearsOfEmployment(endDate_dateTimePicker.Value).ToString();
            }
        }

        /// <summary>
        /// Resets the values currently held by the DateTimePicker controls
        /// </summary>
        private void ResetCalendars()
        {
            startDate_dateTimePicker.Value = startDate_dateTimePicker.MaxDate;
            endDate_dateTimePicker.Value = endDate_dateTimePicker.MaxDate;
        }

        /// <summary>
        /// Unveils certain UI elements
        /// </summary>
        private void ShowElements()
        {
            totalEarnings_label.Visible = true;
            totalEarnings_textBox.Visible = true;
            basePay_label.Visible = true;
            basePay_textBox.Visible = true;
            yearBonus_label.Visible = true;
            yearBonus_textBox.Visible = true;
            referralBonusPay_label.Visible = true;
            referralBonusPay_textBox.Visible = true;
        }

        /// <summary>
        /// Obtains payroll info from Person's CalculatePay method and displays that info in the appropriate fields
        /// </summary>
        private void ShowPayrollResults()
        {
            try
            {
                // A 3-index decimal array is returned
                var calculatedPay = _SelectedPerson.CalculatePay(startDate_dateTimePicker.Value, endDate_dateTimePicker.Value);
                // Two text fields common to all Person subclass objects are updated
                basePay_textBox.Text = calculatedPay[0].ToString("0.##");
                yearBonus_textBox.Text = calculatedPay[1].ToString("0.##");
                // Since employees can't have inferiors, insted of 0, their field displays N/A, which reflects this reality
                if (_SelectedPerson.Position == "Employee")
                {
                    referralBonusPay_textBox.Text = "N/A";
                }
                // All other worker categories can have inferiors, therefore their respective text field displays the actual value
                else
                {
                    referralBonusPay_textBox.Text = calculatedPay[2].ToString("0.##");
                }
                // The last text field is a sum of the three
                totalEarnings_textBox.Text = calculatedPay.Sum().ToString("0.##");
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
        }

        /// <summary>
        /// Loads information about the selected superior's inferiors into the tooltip
        /// </summary>
        private void LoadInferiorsIntoTooltip()
        {
            var sb = new StringBuilder();
            try
            {
                foreach (var person in PersonFetcher.FetchInferiors(_SelectedPerson.MemberID))
                {
                    sb.Append($"{person.MemberID}: {person.FullName} - {person.Position}\n");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load elements");
            }
            ListOfInferiorsTooltip.SetToolTip(displayInferiors_button, sb.ToString());
        }

        /// <summary>
        /// Makes sure that when User closes the window, the application really stops working
        /// </summary>
        private void SinglePayrollForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void switchToMultiPayrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminMultiPayrollForm.GetAdminMultiPayrollForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Terminates the application
        /// </summary>
        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void goToMainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var that = AdminPanelForm.GetAdminPanelForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// Hides this form, gets and shows the requested form, and releases the memory allocated to this form
        /// </summary>
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionLogger.LogAction("logged out", EntityIdHolder.AdminID);
            this.Hide();
            var that = LoginForm.GetLoginForm();
            that.Show();
            this.Dispose();
        }

        /// <summary>
        /// When Admin selects Person from the drop-down list, the info about that Person immediately shows
        /// </summary>
        private void personSelection_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplaySelectedPersonInfo();
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void startDate_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateWorkdays();
            AdjustYearsWithCompany();
        }

        /// <summary>
        /// Every time the date changes, the appropriate text fields reflect that change
        /// </summary>
        private void endDate_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            UpdateWorkdays();
            AdjustYearsWithCompany();
        }

        /// <summary>
        /// Calls the appropriate method and unveils the information fields that at that point contain correct information
        /// </summary>
        private void calculatePayroll_button_Click(object sender, EventArgs e)
        {
            ShowPayrollResults();
            ShowElements();
            ActionLogger.LogAction($"calculated payroll for [worker {_SelectedPerson.MemberID}] for the period between {startDate_dateTimePicker.Value.ToShortDateString()} and {endDate_dateTimePicker.Value.ToShortDateString()}", EntityIdHolder.AdminID);
        }

        /// <summary>
        /// Clears all text fields and re-initializes them
        /// </summary>
        private void clearSelection_button_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            InitializeComponent();
            InitializeComponent2();
            LoadPersons();
        }
    }
}
