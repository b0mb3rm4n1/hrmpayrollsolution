﻿namespace GUI
{
    partial class AdminRemoveWorkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminRemoveWorkerForm));
            this.removeWorker_label = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singlePayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allWorkers_comboBox = new System.Windows.Forms.ComboBox();
            this.displayInferiors_button = new System.Windows.Forms.Button();
            this.numberOfInferiors_textBox = new System.Windows.Forms.TextBox();
            this.yearsWithCompany_textBox = new System.Windows.Forms.TextBox();
            this.baseRate_textBox = new System.Windows.Forms.TextBox();
            this.position_textBox = new System.Windows.Forms.TextBox();
            this.dateOfEmployment_textBox = new System.Windows.Forms.TextBox();
            this.fullName_textBox = new System.Windows.Forms.TextBox();
            this.memberID_textBox = new System.Windows.Forms.TextBox();
            this.supervises_label = new System.Windows.Forms.Label();
            this.yearsWithCompany_label = new System.Windows.Forms.Label();
            this.baseRate_label = new System.Windows.Forms.Label();
            this.position_label = new System.Windows.Forms.Label();
            this.dateOfEmployment_label = new System.Windows.Forms.Label();
            this.fullName_label = new System.Windows.Forms.Label();
            this.memberID_label = new System.Windows.Forms.Label();
            this.removeWorker_button = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // removeWorker_label
            // 
            this.removeWorker_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.removeWorker_label.AutoSize = true;
            this.removeWorker_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeWorker_label.Location = new System.Drawing.Point(39, 46);
            this.removeWorker_label.Name = "removeWorker_label";
            this.removeWorker_label.Size = new System.Drawing.Size(142, 19);
            this.removeWorker_label.TabIndex = 21;
            this.removeWorker_label.Text = "Remove Worker";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.singlePayrollToolStripMenuItem,
            this.multiPayrollToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            this.goToMainMenuToolStripMenuItem.Click += new System.EventHandler(this.goToMainMenuToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // singlePayrollToolStripMenuItem
            // 
            this.singlePayrollToolStripMenuItem.Name = "singlePayrollToolStripMenuItem";
            this.singlePayrollToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.singlePayrollToolStripMenuItem.Text = "SinglePayroll";
            this.singlePayrollToolStripMenuItem.Click += new System.EventHandler(this.singlePayrollToolStripMenuItem_Click);
            // 
            // multiPayrollToolStripMenuItem
            // 
            this.multiPayrollToolStripMenuItem.Name = "multiPayrollToolStripMenuItem";
            this.multiPayrollToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.multiPayrollToolStripMenuItem.Text = "MultiPayroll";
            this.multiPayrollToolStripMenuItem.Click += new System.EventHandler(this.multiPayrollToolStripMenuItem_Click);
            // 
            // allWorkers_comboBox
            // 
            this.allWorkers_comboBox.FormattingEnabled = true;
            this.allWorkers_comboBox.Location = new System.Drawing.Point(239, 46);
            this.allWorkers_comboBox.Name = "allWorkers_comboBox";
            this.allWorkers_comboBox.Size = new System.Drawing.Size(266, 21);
            this.allWorkers_comboBox.TabIndex = 23;
            this.allWorkers_comboBox.Text = "Select...";
            this.allWorkers_comboBox.SelectedIndexChanged += new System.EventHandler(this.allWorkers_comboBox_SelectedIndexChanged);
            // 
            // displayInferiors_button
            // 
            this.displayInferiors_button.BackColor = System.Drawing.Color.DarkGray;
            this.displayInferiors_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayInferiors_button.Location = new System.Drawing.Point(511, 390);
            this.displayInferiors_button.Name = "displayInferiors_button";
            this.displayInferiors_button.Size = new System.Drawing.Size(55, 23);
            this.displayInferiors_button.TabIndex = 44;
            this.displayInferiors_button.Text = "See";
            this.displayInferiors_button.UseVisualStyleBackColor = false;
            this.displayInferiors_button.Visible = false;
            // 
            // numberOfInferiors_textBox
            // 
            this.numberOfInferiors_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfInferiors_textBox.Location = new System.Drawing.Point(239, 390);
            this.numberOfInferiors_textBox.Name = "numberOfInferiors_textBox";
            this.numberOfInferiors_textBox.ReadOnly = true;
            this.numberOfInferiors_textBox.Size = new System.Drawing.Size(266, 23);
            this.numberOfInferiors_textBox.TabIndex = 43;
            this.numberOfInferiors_textBox.Visible = false;
            // 
            // yearsWithCompany_textBox
            // 
            this.yearsWithCompany_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearsWithCompany_textBox.Location = new System.Drawing.Point(239, 345);
            this.yearsWithCompany_textBox.Name = "yearsWithCompany_textBox";
            this.yearsWithCompany_textBox.ReadOnly = true;
            this.yearsWithCompany_textBox.Size = new System.Drawing.Size(266, 23);
            this.yearsWithCompany_textBox.TabIndex = 42;
            // 
            // baseRate_textBox
            // 
            this.baseRate_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseRate_textBox.Location = new System.Drawing.Point(239, 280);
            this.baseRate_textBox.Name = "baseRate_textBox";
            this.baseRate_textBox.ReadOnly = true;
            this.baseRate_textBox.Size = new System.Drawing.Size(266, 23);
            this.baseRate_textBox.TabIndex = 41;
            // 
            // position_textBox
            // 
            this.position_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position_textBox.Location = new System.Drawing.Point(239, 240);
            this.position_textBox.Name = "position_textBox";
            this.position_textBox.ReadOnly = true;
            this.position_textBox.Size = new System.Drawing.Size(266, 23);
            this.position_textBox.TabIndex = 40;
            // 
            // dateOfEmployment_textBox
            // 
            this.dateOfEmployment_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateOfEmployment_textBox.Location = new System.Drawing.Point(239, 197);
            this.dateOfEmployment_textBox.Name = "dateOfEmployment_textBox";
            this.dateOfEmployment_textBox.ReadOnly = true;
            this.dateOfEmployment_textBox.Size = new System.Drawing.Size(266, 23);
            this.dateOfEmployment_textBox.TabIndex = 39;
            // 
            // fullName_textBox
            // 
            this.fullName_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullName_textBox.Location = new System.Drawing.Point(239, 157);
            this.fullName_textBox.Name = "fullName_textBox";
            this.fullName_textBox.ReadOnly = true;
            this.fullName_textBox.Size = new System.Drawing.Size(266, 23);
            this.fullName_textBox.TabIndex = 38;
            // 
            // memberID_textBox
            // 
            this.memberID_textBox.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberID_textBox.Location = new System.Drawing.Point(239, 116);
            this.memberID_textBox.Name = "memberID_textBox";
            this.memberID_textBox.ReadOnly = true;
            this.memberID_textBox.Size = new System.Drawing.Size(266, 23);
            this.memberID_textBox.TabIndex = 37;
            // 
            // supervises_label
            // 
            this.supervises_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supervises_label.AutoSize = true;
            this.supervises_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supervises_label.Location = new System.Drawing.Point(39, 393);
            this.supervises_label.Name = "supervises_label";
            this.supervises_label.Size = new System.Drawing.Size(103, 19);
            this.supervises_label.TabIndex = 36;
            this.supervises_label.Text = "Supervises:";
            this.supervises_label.Visible = false;
            // 
            // yearsWithCompany_label
            // 
            this.yearsWithCompany_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yearsWithCompany_label.AutoSize = true;
            this.yearsWithCompany_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearsWithCompany_label.Location = new System.Drawing.Point(39, 349);
            this.yearsWithCompany_label.Name = "yearsWithCompany_label";
            this.yearsWithCompany_label.Size = new System.Drawing.Size(187, 19);
            this.yearsWithCompany_label.TabIndex = 35;
            this.yearsWithCompany_label.Text = "Years With Company:";
            // 
            // baseRate_label
            // 
            this.baseRate_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseRate_label.AutoSize = true;
            this.baseRate_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baseRate_label.Location = new System.Drawing.Point(39, 280);
            this.baseRate_label.Name = "baseRate_label";
            this.baseRate_label.Size = new System.Drawing.Size(156, 19);
            this.baseRate_label.TabIndex = 34;
            this.baseRate_label.Text = "Base Hourly Rate:";
            // 
            // position_label
            // 
            this.position_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.position_label.AutoSize = true;
            this.position_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position_label.Location = new System.Drawing.Point(39, 244);
            this.position_label.Name = "position_label";
            this.position_label.Size = new System.Drawing.Size(84, 19);
            this.position_label.TabIndex = 33;
            this.position_label.Text = "Position:";
            // 
            // dateOfEmployment_label
            // 
            this.dateOfEmployment_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateOfEmployment_label.AutoSize = true;
            this.dateOfEmployment_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateOfEmployment_label.Location = new System.Drawing.Point(39, 201);
            this.dateOfEmployment_label.Name = "dateOfEmployment_label";
            this.dateOfEmployment_label.Size = new System.Drawing.Size(186, 19);
            this.dateOfEmployment_label.TabIndex = 32;
            this.dateOfEmployment_label.Text = "Date Of Employment:";
            // 
            // fullName_label
            // 
            this.fullName_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fullName_label.AutoSize = true;
            this.fullName_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullName_label.Location = new System.Drawing.Point(39, 158);
            this.fullName_label.Name = "fullName_label";
            this.fullName_label.Size = new System.Drawing.Size(97, 19);
            this.fullName_label.TabIndex = 31;
            this.fullName_label.Text = "Full Name:";
            // 
            // memberID_label
            // 
            this.memberID_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memberID_label.AutoSize = true;
            this.memberID_label.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberID_label.Location = new System.Drawing.Point(39, 116);
            this.memberID_label.Name = "memberID_label";
            this.memberID_label.Size = new System.Drawing.Size(105, 19);
            this.memberID_label.TabIndex = 30;
            this.memberID_label.Text = "Member ID:";
            // 
            // removeWorker_button
            // 
            this.removeWorker_button.BackColor = System.Drawing.Color.Red;
            this.removeWorker_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeWorker_button.Location = new System.Drawing.Point(512, 46);
            this.removeWorker_button.Name = "removeWorker_button";
            this.removeWorker_button.Size = new System.Drawing.Size(77, 23);
            this.removeWorker_button.TabIndex = 44;
            this.removeWorker_button.Text = "Remove";
            this.removeWorker_button.UseVisualStyleBackColor = false;
            this.removeWorker_button.Visible = false;
            this.removeWorker_button.Click += new System.EventHandler(this.removeWorker_button_Click);
            // 
            // AdminRemoveWorkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.removeWorker_button);
            this.Controls.Add(this.displayInferiors_button);
            this.Controls.Add(this.numberOfInferiors_textBox);
            this.Controls.Add(this.yearsWithCompany_textBox);
            this.Controls.Add(this.baseRate_textBox);
            this.Controls.Add(this.position_textBox);
            this.Controls.Add(this.dateOfEmployment_textBox);
            this.Controls.Add(this.fullName_textBox);
            this.Controls.Add(this.memberID_textBox);
            this.Controls.Add(this.supervises_label);
            this.Controls.Add(this.yearsWithCompany_label);
            this.Controls.Add(this.baseRate_label);
            this.Controls.Add(this.position_label);
            this.Controls.Add(this.dateOfEmployment_label);
            this.Controls.Add(this.fullName_label);
            this.Controls.Add(this.memberID_label);
            this.Controls.Add(this.allWorkers_comboBox);
            this.Controls.Add(this.removeWorker_label);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminRemoveWorkerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin Remove Worker";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminRemoveWorkerForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label removeWorker_label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singlePayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPayrollToolStripMenuItem;
        private System.Windows.Forms.ComboBox allWorkers_comboBox;
        private System.Windows.Forms.Button displayInferiors_button;
        private System.Windows.Forms.TextBox numberOfInferiors_textBox;
        private System.Windows.Forms.TextBox yearsWithCompany_textBox;
        private System.Windows.Forms.TextBox baseRate_textBox;
        private System.Windows.Forms.TextBox position_textBox;
        private System.Windows.Forms.TextBox dateOfEmployment_textBox;
        private System.Windows.Forms.TextBox fullName_textBox;
        private System.Windows.Forms.TextBox memberID_textBox;
        private System.Windows.Forms.Label supervises_label;
        private System.Windows.Forms.Label yearsWithCompany_label;
        private System.Windows.Forms.Label baseRate_label;
        private System.Windows.Forms.Label position_label;
        private System.Windows.Forms.Label dateOfEmployment_label;
        private System.Windows.Forms.Label fullName_label;
        private System.Windows.Forms.Label memberID_label;
        private System.Windows.Forms.Button removeWorker_button;
    }
}