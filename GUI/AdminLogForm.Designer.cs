﻿namespace GUI
{
    partial class AdminLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminLogForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goToMainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singlePayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPayrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.log_dataGridView = new System.Windows.Forms.DataGridView();
            this.id_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timestamp_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.action_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getLog_button = new System.Windows.Forms.Button();
            this.clearLog_button = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.log_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.singlePayrollToolStripMenuItem,
            this.multiPayrollToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToMainMenuToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.exitApplicationToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // goToMainMenuToolStripMenuItem
            // 
            this.goToMainMenuToolStripMenuItem.Name = "goToMainMenuToolStripMenuItem";
            this.goToMainMenuToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.goToMainMenuToolStripMenuItem.Text = "Go To Main Menu";
            this.goToMainMenuToolStripMenuItem.Click += new System.EventHandler(this.goToMainMenuToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // singlePayrollToolStripMenuItem
            // 
            this.singlePayrollToolStripMenuItem.Name = "singlePayrollToolStripMenuItem";
            this.singlePayrollToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.singlePayrollToolStripMenuItem.Text = "SinglePayroll";
            this.singlePayrollToolStripMenuItem.Click += new System.EventHandler(this.singlePayrollToolStripMenuItem_Click);
            // 
            // multiPayrollToolStripMenuItem
            // 
            this.multiPayrollToolStripMenuItem.Name = "multiPayrollToolStripMenuItem";
            this.multiPayrollToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.multiPayrollToolStripMenuItem.Text = "MultiPayroll";
            this.multiPayrollToolStripMenuItem.Click += new System.EventHandler(this.multiPayrollToolStripMenuItem_Click);
            // 
            // log_dataGridView
            // 
            this.log_dataGridView.AllowUserToAddRows = false;
            this.log_dataGridView.AllowUserToDeleteRows = false;
            this.log_dataGridView.AllowUserToResizeColumns = false;
            this.log_dataGridView.AllowUserToResizeRows = false;
            this.log_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.log_dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.log_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.log_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_column,
            this.timestamp_column,
            this.action_column});
            this.log_dataGridView.Location = new System.Drawing.Point(22, 53);
            this.log_dataGridView.Name = "log_dataGridView";
            this.log_dataGridView.ReadOnly = true;
            this.log_dataGridView.RowHeadersVisible = false;
            this.log_dataGridView.Size = new System.Drawing.Size(600, 332);
            this.log_dataGridView.TabIndex = 22;
            // 
            // id_column
            // 
            this.id_column.FillWeight = 22.26561F;
            this.id_column.HeaderText = "ID";
            this.id_column.Name = "id_column";
            this.id_column.ReadOnly = true;
            // 
            // timestamp_column
            // 
            this.timestamp_column.FillWeight = 68.52793F;
            this.timestamp_column.HeaderText = "Time Stamp";
            this.timestamp_column.Name = "timestamp_column";
            this.timestamp_column.ReadOnly = true;
            // 
            // action_column
            // 
            this.action_column.FillWeight = 209.2065F;
            this.action_column.HeaderText = "Action";
            this.action_column.Name = "action_column";
            this.action_column.ReadOnly = true;
            // 
            // getLog_button
            // 
            this.getLog_button.Location = new System.Drawing.Point(641, 53);
            this.getLog_button.Name = "getLog_button";
            this.getLog_button.Size = new System.Drawing.Size(126, 51);
            this.getLog_button.TabIndex = 23;
            this.getLog_button.Text = "Get Log";
            this.getLog_button.UseVisualStyleBackColor = true;
            this.getLog_button.Click += new System.EventHandler(this.getLog_button_Click);
            // 
            // clearLog_button
            // 
            this.clearLog_button.Location = new System.Drawing.Point(641, 333);
            this.clearLog_button.Name = "clearLog_button";
            this.clearLog_button.Size = new System.Drawing.Size(126, 52);
            this.clearLog_button.TabIndex = 24;
            this.clearLog_button.Text = "Clear Log";
            this.clearLog_button.UseVisualStyleBackColor = true;
            this.clearLog_button.Click += new System.EventHandler(this.clearLog_button_Click);
            // 
            // AdminLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.clearLog_button);
            this.Controls.Add(this.getLog_button);
            this.Controls.Add(this.log_dataGridView);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AdminLogForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HRMPayroll Admin Log";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminLogForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.log_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToMainMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singlePayrollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPayrollToolStripMenuItem;
        private System.Windows.Forms.DataGridView log_dataGridView;
        private System.Windows.Forms.Button getLog_button;
        private System.Windows.Forms.Button clearLog_button;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn timestamp_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn action_column;
    }
}