﻿using System;

namespace Base
{
    /// <summary>
    /// User action model, defines log entries displayed to the user
    /// </summary>
    public class LogEntry
    {
        /// <summary>UserId is the ID of User who performed the action</summary>
        public int UserId { get; }
        /// <summary>Timestamp is the time when User performed the action</summary>
        public DateTime Timestamp { get; }
        /// <summary>Action is the description of the action User performed</summary>
        public string Action { get; }

        /// <summary>
        /// LogEntry class constructor
        /// </summary>
        /// <param name="userId"> is assigned to UserId</param>
        /// <param name="timestamp"> is assigned to Timestamp</param>
        /// <param name="action"> is assigned to Action</param>
        public LogEntry(int userId, DateTime timestamp, string action)
        {
            UserId = userId;
            Timestamp = timestamp;
            Action = action;
        }
    }
}
