﻿using System;
using Service;

namespace Base
{
    /// <summary>
    /// Person class model, used for passing data to and from the database, serves as the base class to the 3 main classes of the application
    /// </summary>
    public class Person
    {
        /// <summary>MemberID is Person's unique ID</summary>
        public int MemberID { get; }
        /// <summary>FullName is Person's full name</summary>
        public string FullName { get; }
        /// <summary>DateOfEmployment is the date when Person joined the company</summary>
        public DateTime DateOfEmployment { get; }
        /// <summary>Position is the position currently held by Person</summary>
        public string Position { get; }
        /// <summary>BaseHourlyRate is how much money Person makes per hour</summary>
        public decimal BaseHourlyRate { get; }
        /// <summary>YearsOfEmploymentByPayrollsDate is the number of years Person had worked for by the end of the payroll end date</summary>
        public int YearsOfEmploymentByPayrollsDate { get; set; }
        /// <summary>PayrollStartDate is the actual start date from which Person's payroll can be calculated</summary>
        public DateTime PayrollStartDate { get; set; }
        /// <summary>PayrollEndDate is the actual start date to which Person's payroll can be calculated</summary>
        public DateTime PayrollEndDate { get; set; }
        /// <summary>WorkHoursPerDay is the constant number of hours Worker works in one day</summary>
        public const int WorkHoursPerDay = 8;

        /// <summary>
        /// Person class constructor
        /// </summary>
        /// <param name="memberID"> is assigned to MemberID</param>
        /// <param name="fullName"> is assigned to MemberID</param>
        /// <param name="dateOfEmployment"> is assigned to DateOfEmployment</param>
        /// <param name="position"> is assigned to Position</param>
        /// <param name="baseRate"> is assigned to BaseHourlyRate</param>
        public Person(int memberID, string fullName, DateTime dateOfEmployment, string position, decimal baseRate)
        {
            MemberID = memberID;
            FullName = fullName;
            DateOfEmployment = dateOfEmployment;
            Position = position;
            BaseHourlyRate = baseRate;
        }

        /// <summary>
        /// Counts how many years of employment Person had worked for by the incoming date
        /// </summary>
        /// <param name="date">Date by which the exact number of employment years is needed</param>
        /// <returns>The number of years between the specified date and the object's DateOfEmployment property</returns>
        public int GetYearsOfEmployment(DateTime date)
        {
            return (int)((date - DateOfEmployment).TotalDays / 365);
        }

        /// <summary>
        /// Calculates pay for the period between two dates (basic implementation here because this method gets overridden anyway)
        /// </summary>
        /// <param name="startDate">date from</param>
        /// <param name="endDate">date to</param>
        /// <returns>A 3-decimal array, each element of which is displayed separately</returns>
        public virtual decimal[] CalculatePay(DateTime startDate, DateTime endDate)
        {
            return new decimal[] { -1, -1, -1 };
        }

        /// <summary>
        /// Adjusts the object's PayrollStartDate and PayrollEndDate properties and returns the number of workdays spanning the two incoming dates
        /// </summary>
        /// <param name="startDate">date from</param>
        /// <param name="endDate">date to</param>
        /// <returns>The number of workdays sandwiched between the incoming dates</returns>
        public int AdjustTime(DateTime startDate, DateTime endDate)
        {
            int workDays = default(int);

            // 1) the properties don't change if the worker's DateOfEmployment is not in the given range
            if (DateOfEmployment > startDate && DateOfEmployment > endDate)
            {
                workDays = 0;
            }
            // 2) if Worker was employed in between the dates, PayrollStartDate becomes DateOfEmployment, PayrollEndDate assumes the expected value
            else if (DateOfEmployment > startDate && DateOfEmployment < endDate)
            {
                // and we calculate what's left of that period
                PayrollStartDate = DateOfEmployment;
                PayrollEndDate = endDate;
                workDays = WorkdayCounter.CountWorkdays(PayrollStartDate, PayrollEndDate);
            }
            // 3) both merge with the incoming dates if the requested period is one workday
            else if (startDate == endDate && WorkdayCounter.CountWorkdays(startDate, endDate) == 1)
            {
                // and we know that it is just one workday
                PayrollStartDate = startDate;
                PayrollEndDate = endDate;
                workDays = 1;
            }
            // in all other cases, the object's properties assume both of the incoming values and the method counts workdays for the full period
            else
            {
                PayrollStartDate = startDate;
                PayrollEndDate = endDate;
                workDays = WorkdayCounter.CountWorkdays(PayrollStartDate, PayrollEndDate);
            }
            // Adjusts the object's YearsOfEmploymentByPayrollsDate property
            YearsOfEmploymentByPayrollsDate = GetYearsOfEmployment(PayrollEndDate);
            return workDays;
        }
    }
}
