﻿namespace Base
{
    /// <summary>
    /// User class model, used for user authorization and authentication
    /// </summary>
    public class User
    {
        /// <summary>UserID is an int that represents unique user number and is used to link DB tables together</summary>
        public int UserID { get; }
        /// <value>Login is a string used as a username used during user authorization</summary>
        public string Login { get; }
        /// <value>Password is a string used as a password during user authorization</summary>
        public string Password { get; }

        /// <summary>
        /// User class constructor
        /// </summary>
        /// <param name="login">is linked to the Login property</param>
        /// <param name="password"> is linked to the Password property </param>
        /// <param name="id"> is linked to the UserID property</param>
        public User(string login, string password, int id)
        {
            Login = login;
            Password = password;
            UserID = id;
        }
    }
}
