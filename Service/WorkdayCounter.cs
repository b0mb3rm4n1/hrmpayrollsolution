﻿using System;

namespace Service
{
    /// <summary>
    /// Static utility class used for the counting of workdays
    /// </summary>
    public static class WorkdayCounter
    {
        /// <summary>
        /// Takes two dates and counts business/working days between them
        /// </summary>
        /// <param name="startDate">date from which workdays will be counted</param>
        /// <param name="endDate">date to which workdays will be counted</param>
        /// <returns>number of business days contained in full weeks + the variable number of business days contained in the remainder of the time span</returns>
        public static int CountWorkdays(DateTime startDate, DateTime endDate)
        {
            // Finds out what day of the week it is and represents it as an integer
            int dayOfWeekStart = ((int)startDate.DayOfWeek == 0 ? 7 : (int)startDate.DayOfWeek);
            int dayOfWeekEnd = ((int)endDate.DayOfWeek == 0 ? 7 : (int)endDate.DayOfWeek);
            // Measures how much time is contained between the two dates
            TimeSpan tSpan = endDate - startDate;
            // Depending on which day of the week it is, the workdays are counted
            if (dayOfWeekStart <= dayOfWeekEnd)
            {
                return (((tSpan.Days / 7) * 5) + Math.Max((Math.Min((dayOfWeekEnd + 1), 6) - dayOfWeekStart), 0));
            }
            else
            {
                return (((tSpan.Days / 7) * 5) + Math.Min((dayOfWeekEnd + 6) - Math.Min(dayOfWeekStart, 6), 5));
            }
        }
    }
}
