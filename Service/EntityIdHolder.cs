﻿namespace Service
{
    /// <summary>
    /// Static utility class that holds Admin and Worker ID values
    /// </summary>
    public static class EntityIdHolder
    {
        /// <summary>WorkerID represents Worker's ID, used to personalize WorkerPayrollForm</summary>
        /// <value>The default value (-1) represents unknown User</value>
        public static int WorkerID { get; set; } = -1;

        /// <summary>AdminID is used to log admin actions, replaces the literal value of 0</summary>
        /// <value>AdminID is 0</value>
        public static int AdminID { get; } = 0;
    }
}
