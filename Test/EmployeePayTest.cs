﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core;

namespace Test
{
    [TestClass]
    public class EmployeePayTest
    {
        [TestMethod]
        public void EmployeeOneWorkdayPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2019-05-01"), "Employee", 10);
            // Act: Friday, business/workday, should be 10 * 8 = 80
            var pay = employee.CalculatePay(DateTime.Parse("2019-05-10"), DateTime.Parse("2019-05-10")).Sum();
            // Assert
            Assert.AreEqual(80, pay);
        }

        [TestMethod]
        public void EmployeeOneWeekPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2019-05-01"), "Employee", 10);
            // Act: One week OR 5 workdays, should be 80 * 5 = 400
            var pay = employee.CalculatePay(DateTime.Parse("2019-05-01"), DateTime.Parse("2019-05-07")).Sum();
            // Assert
            Assert.AreEqual(400, pay);
        }

        [TestMethod]
        public void EmployeePayForDayBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2019-05-01"), "Employee", 10);
            // Act: Wednesday, business day, before the day the employee was employed, should be 0
            var pay = employee.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-01")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void EmployeePayForMonthBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2019-05-01"), "Employee", 10);
            // Act: Employee was employed on May 1. We calculate their pay for January, 3 months before the employment happened, should be 0
            var pay = employee.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void EmployeePayForMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2000-01-01"), "Employee", 10);
            // Act: Employee was employed on Jan 1 2000. As of May 10, 2019, that Employee is supposed to receive receive 19 * 3% (per year) = 57% in bonus
                // but the cap is 30%, so Employee should receive 23 (workdays in Jan 2019) * 8 * 10 = 1840  + 552 (30% of that amount in bonuses) = 2392
            var pay = employee.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual(2392, pay);
        }

        [TestMethod]
        public void EmployeePayForCustomMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var employee = new Employee(0, "Employee One", DateTime.Parse("2000-01-01"), "Employee", 10);
            // Act: Employee was employed on Jan 1 2000. Today he has 19 of years of employment which gives him the highest possible yearly bonus
                // But in May 2005 that employee had only 5 years or 5 * 3 = 15% bonus pay, let's check how much he earned in the same month of 2005
                // it should be 21 (business days in Jan 2005) * 10 * 8 = 1680 + 252 (15% of that amount in bonuses) = 1932
            var pay = employee.CalculatePay(DateTime.Parse("2005-01-01"), DateTime.Parse("2005-01-31")).Sum();
            // Assert
            Assert.AreEqual(1932, pay);
        }
    }
}
