﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core;

namespace Test
{
    [TestClass]
    public class ManagerPayTest
    {
        // Manager without inferiors
        [TestMethod]
        public void ManagerAloneOneWorkdayPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2019-05-01"), "Manager", 20);
            // Act: Friday, business/workday, should be 20 * 8 = 160
            var pay = manager.CalculatePay(DateTime.Parse("2019-05-10"), DateTime.Parse("2019-05-10")).Sum();
            // Assert
            Assert.AreEqual(160, pay);
        }

        [TestMethod]
        public void ManagerAloneOneWeekPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2019-05-01"), "Manager", 20);
            // Act: One week OR 5 workdays, should be 160 * 5 = 800
            var pay = manager.CalculatePay(DateTime.Parse("2019-05-01"), DateTime.Parse("2019-05-07")).Sum();
            // Assert
            Assert.AreEqual(800, pay);
        }

        [TestMethod]
        public void ManagerAlonePayForDayBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2019-05-01"), "Manager", 20);
            // Act: Wednesday, business day, before the day Manager was employed, should be 0
            var pay = manager.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-01")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void ManagerAlonePayForMonthBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2019-05-01"), "Manager", 20);
            // Act: Manager was employed on May 1, we calculate their pay for January, 3 months before the employment happened, should be 0
            var pay = manager.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void ManagerAlonePayForMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2000-01-01"), "Manager", 20);
            // Act: The manager was employed on Jan 1 2000. As of May 10, 2019, that Manager is supposed to receive 19 * 5% (per year) = 95% in bonus
                // but the cap is 40%, so Manager should receive 23 (workdays in Jan 2019) * 8 * 20 = 3680  + 1472 (40% of that amount in bonuses) = 5152
            var pay = manager.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual(5152, pay);
        }

        [TestMethod]
        public void ManagerAlonePayForCustomMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var manager = new Manager(0, "Manager One", DateTime.Parse("2000-01-01"), "Manager", 20);
            // Act: Manager was employed on Jan 1 2000. Today he has 19 of years of employment which gives him the highest possible yearly bonus
                // But in May 2005 that employee had only 5 years or 5*5 = 25% bonus pay, let's check how much he earned in the same month of 2005
                // it should be 21 (business days in Jan 2005) * 20 * 8 = 3360 + 840 (25% of that amount in bonuses) = 4200
            var pay = manager.CalculatePay(DateTime.Parse("2005-01-01"), DateTime.Parse("2005-01-31")).Sum();
            // Assert
            Assert.AreEqual(4200, pay);
        }

        // Manager with inferiors
        [TestMethod]
        public void ManagerWithInferiorsAbsoluteOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
                // Manager and all of their inferiors were employed before the payroll start date
            var manager = new Manager(0, "Manager One", DateTime.Parse("2000-01-01"), "Manager", 20); // 19 years * 5% = 95% => 40% bonus (cap)
            var employee1 = new Employee(1, "Employee One", DateTime.Parse("2005-01-01"), "Employee", 10); // 14 years * 3% = 42% => 30% bonus (cap)
            var employee2 = new Employee(2, "Employee Two", DateTime.Parse("2010-01-01"), "Employee", (decimal)12.5); // 9 years * 3% = 27% => 27% bonus
            // Act: Manager made in January 2019 (23 workdays) = 23 * 20 * 8 = 3680 + 1472 (40% of that in yearly bonus) = 5152
                // + 26.565 (0.5% of what all his inferiors made) = 5178.565
                    // Employee1 made = 23 * 10 * 8 = 1840 + 552 (30% of that in yearly bonus) = 2392
                    // Employee2 made = 23 * 12.5 * 8 = 2300 + 621 (27% of that in yearly bonus) = 2921
            manager.ListOfInferiorEmployees.Add(employee1);
            manager.ListOfInferiorEmployees.Add(employee2);
            var pay = manager.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual((decimal)5178.565, pay);
        }

        [TestMethod]
        public void ManagerWithInferiorsPartialOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
                // Manager and only one of their inferiors were employed before the payroll start date
            var manager = new Manager(0, "Manager One", DateTime.Parse("2000-01-01"), "Manager", 20); // 7 years (as of March 2007) * 5% = 35% => 35% bonus
            var employee1 = new Employee(1, "Employee One", DateTime.Parse("2005-01-01"), "Employee", (decimal)17.33); // 2 years (as of March 2007) * 3% = 6% => 6% bonus
            var employee2 = new Employee(2, "Employee Two", DateTime.Parse("2010-01-01"), "Employee", (decimal)12.5); // 0 years (as of March 2007) * 3% = 0% => 0% bonus
            // Act: Manager made in March 2007 (22 workdays) = 22 * 20 * 8 = 3520 + 1232 (35% of that in yearly bonus) = 4752
                // + 16.165424 (0.5% of what all his inferiors made) = 4768.165424
                    // Employee1 made = 22 * 17.33 * 8 = 3050.08 + 183.0048 (6% of that in yearly bonus) = 3233.0848
                    // Employee2 didn't make anything since he or she wasn't employed at that time
            manager.ListOfInferiorEmployees.Add(employee1);
            manager.ListOfInferiorEmployees.Add(employee2);
            var pay = manager.CalculatePay(DateTime.Parse("2007-03-01"), DateTime.Parse("2007-03-31")).Sum();
            // Assert
            Assert.AreEqual((decimal)4768.165424, pay);
        }

        [TestMethod]
        public void ManagerWithInferiorsWithoutOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
                // Nobody worked within the specified pay period
            var manager = new Manager(0, "Manager One", DateTime.Parse("2000-01-01"), "Manager", 20);
            var employee1 = new Employee(1, "Employee One", DateTime.Parse("2005-01-01"), "Employee", (decimal)17.33);
            var employee2 = new Employee(2, "Employee Two", DateTime.Parse("2010-01-01"), "Employee", (decimal)12.5);
            // Act: Nobody made anything in January 1999 (21 workdays), we expect 0
            manager.ListOfInferiorEmployees.Add(employee1);
            manager.ListOfInferiorEmployees.Add(employee2);
            var pay = manager.CalculatePay(DateTime.Parse("1999-01-01"), DateTime.Parse("1999-01-31")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }
    }
}