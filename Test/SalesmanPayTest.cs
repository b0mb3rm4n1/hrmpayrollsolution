﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core;

namespace Test
{
    [TestClass]
    public class SalesmanPayTest
    {
        // Salesman without inferiors
        [TestMethod]
        public void SalesmanAloneOneWorkdayPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2019-05-01"), "Salesman", 30);
            // Act: Friday, business/workday, should be 30 * 8 = 240
            var pay = salesman.CalculatePay(DateTime.Parse("2019-05-10"), DateTime.Parse("2019-05-10")).Sum();
            // Assert
            Assert.AreEqual(240, pay);
        }

        [TestMethod]
        public void SalesmanAloneOneWeekPay()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2019-05-01"), "Salesman", 30);
            // Act: One week OR 5 workdays, should be 240 * 5 = 1200
            var pay = salesman.CalculatePay(DateTime.Parse("2019-05-01"), DateTime.Parse("2019-05-07")).Sum();
            // Assert
            Assert.AreEqual(1200, pay);
        }

        [TestMethod]
        public void SalesmanAlonePayForDayBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2019-05-01"), "Salesman", 30);
            // Act: Wednesday, business day, before the day the salesman was employed, should be 0
            var pay = salesman.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-01")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void SalesmanAlonePayForMonthBeforeEmployment()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2019-05-01"), "Salesman", 30);
            // Act: The Salesman was employed on May 1, we calculate their pay for January, 3 months before the employment happened, should be 0
            var pay = salesman.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }

        [TestMethod]
        public void SalesmanAlonePayForMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2000-01-01"), "Salesman", 30);
            // Act: Salesman was employed on Jan 1 2000. As of May 10, 2019, that Salesman is supposed to receive 19 * 1% (per year) = 19% in bonus
                // which does not exceed the 35% cap, so Salesman should receive 23 (workdays in Jan 2019) * 8 * 30 = 5520  + 1040.8 (19% of that amount in bonuses) = 6568.8
            var pay = salesman.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual((decimal)6568.8, pay);
        }

        [TestMethod]
        public void SalesmanAlonePayForCustomMonthWithYearlyBonus()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2000-01-01"), "Salesman", 30);
            // Act: Salesman was employed on Jan 1, 2000. Today he has 19 of years of employment which gives him 19% in yearly bonus.
                // But in May 2005 that Salesman had only 5 years or 5 * 1 = 5% bonus pay, let's check how much he earned in the same month of 2005:
                // it should be 21 (business days in Jan 2005) * 30 * 8 = 5040 + 252 (5% of that amount in bonuses) = 5292
            var pay = salesman.CalculatePay(DateTime.Parse("2005-01-01"), DateTime.Parse("2005-01-31")).Sum();
            // Assert
            Assert.AreEqual(5292, pay);
        }

        // Salesman with inferiors
        [TestMethod]
        public void SalesmanWithInferiorsAbsoluteOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
                // Salesman and all inferiors (different kinds: Employee and Manager) were employed before the payroll start date
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2000-01-01"), "Salesman", (decimal)33.3); // 19 years * 1% = 19% => 19% bonus
            var employee = new Employee(1, "Employee One", DateTime.Parse("2005-01-01"), "Employee", (decimal)11.1); // 14 years * 3% = 42% => 30% bonus (cap)
            var manager = new Manager(2, "Manager One", DateTime.Parse("2010-01-01"), "Manager", (decimal)12.5); // 9 years * 5% = 45% => 40% bonus (cap)
            // Act: Salesman made in January 2019 (23 workdays) = 23 * 33.3 * 8 = 6127.2 + 1164.168 (19% of that in yearly bonus) = 7291.368
                    // Employee made = 23 * 10 * 8 = 2042.4 + 612.72 (30% of that in yearly bonus) = 2655.12
                // but Manager has their own inferior Employee
                var managersEmployee = new Employee(11, "Employee Two", DateTime.Parse("2009-07-01"), "Employee", (decimal)13.4); // 9 years (as of May 10, 2019) * 3 => 27% bonus
                manager.ListOfInferiorEmployees.Add(managersEmployee);
                // we have to include that Employee's pay too, which is 23 * 13.4 * 8 = 2465.6 + 665.712 (27% of that in yearly bonus) = 3131.312, Manager gets 0.5% = 15.65656
                var managersEmployeePay = managersEmployee.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // so, Salesman's Manager made 23 * 12.5 * 8 = 2300 + 920 (40% of that in yearly bonus) + 15.65656 (0.5% of Manager's inferior employee) = 3235.65656
            salesman.ListOfInferiorPersons.Add(employee);
            salesman.ListOfInferiorPersons.Add(manager);
            // Salesman made 7291.368 + 7.96536 (0.3% of what's their Employee made) + 9.70696968 (0.3% of what's their Manager made) = 7309.04032968
            var pay = salesman.CalculatePay(DateTime.Parse("2019-01-01"), DateTime.Parse("2019-01-31")).Sum();
            // Assert
            Assert.AreEqual((decimal)7309.04032968, pay);
        }

        [TestMethod]
        public void SalesmanWithInferiorsPartialOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            // Salesman and only one of their inferiors were employed before the payroll start date
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2000-01-01"), "Salesman", (decimal)33.3); // 7 years (as of March 2007) * 1% = 7% => 7% bonus
            var employee = new Employee(1, "Employee One", DateTime.Parse("2010-01-01"), "Employee", (decimal)11.1); // 0 years (as of March 2007) * 0% = 0% => 0% bonus
            var manager = new Manager(2, "Manager One", DateTime.Parse("2003-01-01"), "Manager", (decimal)12.5); // 4 years (as of March 2007) * 5% = 20% => 20% bonus
            // Act: Salesman made in March 2007 (22 workdays) = 22 * 33.3 * 8 = 5860.8 + 410.256 (7% of that in yearly bonus) = 6271.056
                // Employee didn't make anything since he or she wasn't employed at that time
                // but Manager has their own inferior Employee
                var managersEmployee = new Employee(22, "Employee Two", DateTime.Parse("2005-07-01"), "Employee", (decimal)13.4); // 1 year (as of as of March 2007) * 3 => 3% bonus
                manager.ListOfInferiorEmployees.Add(managersEmployee);
                // we have to include that Employee's pay too, which is 22 * 13.4 * 8 = 2358.4 + 70.752 (3% of that in yearly bonus) = 2429.152, manager gets 0.5% = 12.14576
                var managersEmployeePay = managersEmployee.CalculatePay(DateTime.Parse("2007-03-01"), DateTime.Parse("2007-03-31")).Sum();
            // so, Salesman's Manager made 22 * 12.5 * 8 = 2200 + 440 (20% of that in yearly bonus) + 12.14576 (0.5% of Manager's inferior Employee) = 2652.14576
            salesman.ListOfInferiorPersons.Add(employee);
            salesman.ListOfInferiorPersons.Add(manager);
            // Salesman made 6271.056 + 0 (0.3% of what's their Employee made) + 7.95643728 (0.3% of what's their Manager made) = 6279.01243728
            var pay = salesman.CalculatePay(DateTime.Parse("2007-03-01"), DateTime.Parse("2007-03-31")).Sum();
            // Assert
            Assert.AreEqual((decimal)6279.01243728, pay);
        }

        [TestMethod]
        public void SalesmanWithInferiorsWithoutOverlapWithPayPeriod()
        {
            // Arrange: Base Rate is measured in $/hour, 1 workday = 8 hours
            // Nobody worked within the specified pay period
            var salesman = new Salesman(0, "Salesman One", DateTime.Parse("2000-01-01"), "Salesman", (decimal)33.3);
            var employee = new Employee(1, "Employee One", DateTime.Parse("2010-01-01"), "Employee", (decimal)11.1);
            var manager = new Manager(2, "Manager One", DateTime.Parse("2003-01-01"), "Manager", (decimal)12.5);
            // Act: Nobody made anything in January 1999 (21 workdays), we expect 0
            manager.ListOfInferiorEmployees.Add(employee);
            manager.ListOfInferiorEmployees.Add(employee);
            var pay = manager.CalculatePay(DateTime.Parse("1999-01-01"), DateTime.Parse("1999-01-31")).Sum();
            // Assert
            Assert.AreEqual(0, pay);
        }
    }
}
