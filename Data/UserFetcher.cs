﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Data.SQLite;
using Dapper;
using Base;

namespace Data
{
    /// <summary>
    /// Static utility class used for user authorization
    /// </summary>
    public static class UserFetcher
    {
        /// <summary>Path to the database relative to the executable solution file</summary>
        private const string dbPath = "Data Source=..\\..\\..\\DB\\HRMSystemDB.db;Version=3";

        /// <summary>
        /// Checks if the incoming login and password pair is valid by creating a User object, If the returned list is empty the pair is invalid. 
        /// </summary>
        /// <param name="login">user login</param>
        /// <param name="password">user password</param>
        /// <returns>A list of user objects whose credentials were provided in the arguments and match those stored in the DB</returns>
        public static List<User> CheckUserCredentials(string login, string password)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<User>($"SELECT Login, Password, ID FROM Auth WHERE Login = '{login}' AND Password = '{password}'").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Assigns the specified login and password to a newly created user with the specified ID
        /// </summary>
        /// <param name="id">User/Worker/Person ID</param>
        /// <param name="login">Login</param>
        /// <param name="password">Password</param>
        public static void AssignCredentialsToNewUser(int id, string login, string password)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    db.Execute($"INSERT INTO Auth(ID, Login, Password) VALUES('{id}', '{login}', '{password}')");
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }




    }
}
