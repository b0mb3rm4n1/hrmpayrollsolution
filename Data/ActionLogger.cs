﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SQLite;
using Dapper;
using Base;

namespace Data
{
    /// <summary>
    /// Static utility class with methods that write, read, and remove information about user actions to/from the DB
    /// </summary>
    public static class ActionLogger
    {
        /// <summary>Path to the database relative to the executable solution file</summary>
        private const string dbPath = "Data Source=..\\..\\..\\DB\\HRMSystemDB.db;Version=3";

        /// <summary>
        /// Adds a record containing action description and userId to the DB, timestamp is set by the DB in UTC format
        /// </summary>
        /// <param name="action">Description of User's action</param>
        /// <param name="userId">ID of User who performed the action</param>
        public static void LogAction(string action, int userId)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    db.Execute($"INSERT INTO Log(UserID, Timestamp, Action) VALUES('{userId}', CURRENT_TIMESTAMP, '{action}')");
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches all log records from the DB
        /// </summary>
        /// <returns> A list of LogEntry objects</returns>
        public static List<LogEntry> RetrieveLogRecords()
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<LogEntry>("SELECT * FROM Log").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Removes all log entries stored in the DB table
        /// </summary>
        /// <returns> A boolean indicating if Log was successfully cleared </returns>
        public static bool ClearLog()
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    db.Execute("DELETE FROM Log");
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
