﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using Dapper;
using Base;

namespace Data
{
    /// <summary>
    /// Static utility class with methods that pass information about Persons(Workers) to/from the DB
    /// </summary>
    public static class PersonFetcher
    {
        /// <summary>Path to the database relative to the executable solution file</summary>
        private const string dbPath = "Data Source=..\\..\\..\\DB\\HRMSystemDB.db;Version=3";

        /// <summary>
        /// Fetches all Person objects whose information is stored in the DB
        /// </summary>
        /// <returns>A list of all existing Persons</returns>
        public static List<Person> FetchAll()
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<Person>("SELECT * FROM AllStaff ORDER BY MemberID").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches a list containing one Person object with the specified ID
        /// </summary>
        /// <param name="personID">ID of Person whose information is requested</param>
        /// <returns>A list with a Person object whose ID was sent in</returns>
        public static List<Person> FetchPerson(int personID)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<Person>($"SELECT * FROM AllStaff AS A WHERE A.MemberID = '{personID}'").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches all inferiors assigned to the superior with the specified ID
        /// </summary>
        /// <param name="superiorID">ID of the superior whose inferiors are requested</param>
        /// <returns>A list of Person objects assigned to the superior with the specified ID</returns>
        public static List<Person> FetchInferiors(int superiorID)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<Person>(
                        $"SELECT * " +
                        $"FROM AllStaff AS A " +
                        $"WHERE A.MemberID IN" +
                        $"(SELECT PS.InferiorID FROM PowerStructure AS PS INNER JOIN AllStaff AS A ON PS.MemberID = A.MemberID WHERE PS.MemberID = '{superiorID}')"
                        ).ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches all Person objects holding the specified position
        /// </summary>
        /// <param name="supervisorPosition">Middle/Senior Position that allows assigning of new Workers in inferior positions</param>
        /// <returns>A list of Persons/Workers to whom new Workers in inferior positions can be assigned</returns>
        public static List<Person> FetchPotentialSupervisors(string supervisorPosition)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<Person>($"SELECT * FROM AllStaff WHERE Position = {supervisorPosition}").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches a one-element list containing the maximum Person ID contained in the DB
        /// </summary>
        /// <returns>A list containing the current highest WorkerID number</returns>
        public static List<int> FetchMaxId()
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<int>("SELECT MAX(MemberID) FROM AllStaff").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Fetches all Worker Positions mentioned in the DB
        /// </summary>
        /// <returns>A list of Positions of currently employed Workers as strings</returns>
        public static List<string> FetchPositions()
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    return db.Query<string>("SELECT Position FROM AllStaff GROUP BY Position").ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Adds a new Person entry with the specified values to the DB
        /// </summary>
        /// <param name="id">ID of new Person/Worker</param>
        /// <param name="fullName">Full name of that Person</param>
        /// <param name="dateOfEmployment">Date when that Person officially joined the company</param>
        /// <param name="position">Person's currently held position in the company</param>
        /// <param name="baseRate">How much Person is paid per hour</param>
        /// <returns>A boolean indicating if New Worker was successfully added </returns>
        public static bool AddNewWorker(int id, string fullName, DateTime dateOfEmployment, string position, decimal baseRate)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    db.Execute($"INSERT INTO AllStaff(MemberID, FullName, DateOfEmployment, Position, BaseRate) VALUES('{id}', '{fullName}', strftime('%Y-%m-%d', '{dateOfEmployment.ToString("yyyy-MM-dd")}'), '{position}', '{baseRate}')");
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Adds an entry describing inferiority-superiority between the two incoming Person IDs
        /// If the new worker has a superior, two entries are added to the table, if no superior is specified, only one new entry is added to the table
        /// </summary>
        /// <param name="inferiorID">ID of newly added Person</param>
        /// <param name="superiorID">ID of Person to whom new Person is assigned as inferior, if the value is not provided it means that no superior is assigned</param>
        public static void AssignInferiorToSuperior(int inferiorID, int superiorID = -1)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    if (superiorID != -1)
                    {
                        // The new worker (as inferior) and their superior
                        db.Execute($"INSERT INTO PowerStructure(MemberID, InferiorID) VALUES('{superiorID}', '{inferiorID}')");
                        // The new worker (as superior) with no inferior
                        db.Execute($"INSERT INTO PowerStructure(MemberID, InferiorID) VALUES('{inferiorID}', NULL)"); 
                    }
                    else
                    {
                        // The new worker (as superior) with no inferior
                        db.Execute($"INSERT INTO PowerStructure(MemberID, InferiorID) VALUES('{inferiorID}', NULL)");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not access the database");
                }
            }
        }

        /// <summary>
        /// Removes all mentions of Worker with the specified ID
        /// </summary>
        /// <param name="workerID">ID of Worker whose information to delete</param>
        /// <returns>A boolean indicating if Worker was successfully removed </returns>
        public static bool RemoveWorker(int workerID)
        {
            using (IDbConnection db = new SQLiteConnection(dbPath))
            {
                try
                {
                    db.Execute($"DELETE FROM AllStaff AS Al WHERE Al.MemberID = '{workerID}'");
                    db.Execute($"DELETE FROM Auth AS Au WHERE Au.ID = '{workerID}'");
                    db.Execute($"DELETE FROM PowerStructure AS Ps WHERE Ps.MemberID = '{workerID}' OR Ps.InferiorID = '{workerID}'");
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
